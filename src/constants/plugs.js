export default [
  {
    href: 'https://www.instagram.com/bornwhitfield',
    name: 'Instagram',
    className: 'Instagram',
  },
  {
    href: 'https://dribbble.com/bornwhitfield',
    name: 'Dribbble',
    className: 'Dribbble',
  },
  {
    href: 'https://www.behance.net/bornwhitfield',
    name: 'Behance',
    className: 'Behance',
  },
  {
    href: 'https://twitter.com/darkristy',
    name: 'Twitter',
    className: 'Twitter',
  },
  {
    href: 'https://github.com/darkristy',
    name: 'Github',
    className: 'Github',
  },
]
