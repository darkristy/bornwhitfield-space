export const experiences = [
  {
    key: '',
    year: 'SEP 2019 — DEC 2019',
    position: 'Internship - Port City Media Co.',
  },
  {
    key: '',
    year: 'JAN 2020 — PRESENT',
    position:
      'Independent Design & Development Contractor - Port City Media Co.',
  },
].map(experience => {
  experience.key = `entry-${experience.year}-${experience.position}`
  return experience
})

export const tools = [
  { name: 'Photoshop' },
  { name: 'Illustrator' },
  { name: 'XD' },
  { name: 'Figma' },
  { name: 'Lightroom' },
  { name: 'VSCode' },
]

export const experties = [
  { name: 'Frontend Development' },
  { name: 'Logos' },
  //   { name: 'Brand Identity' },
  { name: 'Illustration' },
  { name: 'Web Design' },
  { name: 'Photography' },
]
