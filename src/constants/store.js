//Hover Image Effect Data

import disp from '../../assets/misc/4.png'
import image1 from '../../assets/misc/Polar1.png'
import image2 from '../../assets/misc/Polar2.png'

// ----Hover Image Data Structure
// [url1, url2, disp, ratio, ....else]

export default [[image1, image2, disp, [8, 8]]]
