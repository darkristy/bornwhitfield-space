export default [
  {
    name: 'Austin Distel',
    plug: 'https://unsplash.com/@austindistel',
  },
  {
    name: 'Fredy Jacob',
    plug: 'https://unsplash.com/@thefredyjacob',
  },
  {
    name: 'Denys Nevozhai',
    plug: 'https://unsplash.com/@dnevozhai',
  },
  {
    name: 'NeONBRAND',
    plug: 'https://unsplash.com/@neonbrand',
  },
  {
    name: 'Merakist',
    plug: 'https://unsplash.com/@merakist',
  },
  {
    name: 'Kelly Sikkema',
    plug: 'https://unsplash.com/@kellysikkema',
  },
  {
    name: 'Ammar Elamir',
    plug: 'https://unsplash.com/@ammarelamir',
  },
  {
    name: 'Arian Darvishi',
    plug: 'https://unsplash.com/@arianismmm',
  },
  {
    name: 'Bench Accounting',
    plug: 'https://unsplash.com/@benchaccounting',
  },
  {
    name: 'Christopher Gower',
    plug: 'https://unsplash.com/@cgower',
  },
  {
    name: 'Hal Gatewood',
    plug: 'https://unsplash.com/@halgatewood',
  },
  {
    name: 'Markus Spiske',
    plug: 'https://unsplash.com/@markusspiske',
  },
  {
    name: 'Med Badr Chemmaoui',
    plug: 'https://unsplash.com/@medbadrc',
  },
  {
    name: 'Raphael Schaller',
    plug: 'https://unsplash.com/@raphaelphotoch',
  },
  {
    name: 'Patrick Perkins',
    plug: 'https://unsplash.com/@pperkins',
  },
  {
    name: 'UX Store',
    plug: 'https://unsplash.com/@uxstore',
  },
]
