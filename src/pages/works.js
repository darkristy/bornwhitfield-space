import React, { useRef, useEffect } from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import styled from 'styled-components'
import Work from '../components/works/work'
import SEO from '../components/seo'
import PagePagination from '../components/pagination/page-pagination'
import PropTypes from 'prop-types'
import { Container } from '../utils/style'

const WorkList = ({ data, pageContext }) => {
  const pathPrefix = `/work/`
  // let workRef = useRef(null)
  // const windowGlobal = typeof window !== 'undefined' && window

  // useEffect(() => {
  //   const content = workRef
  //   let currentPos = windowGlobal.pageYOffset
  //   const callDistort = function() {
  //     const newPos = windowGlobal.pageYOffset
  //     const diff = newPos - currentPos
  //     const speed = diff * 0.2
  //     content.style.transform = 'skewY(' + speed + 'deg)'
  //     currentPos = newPos
  //     requestAnimationFrame(callDistort)
  //   }
  //   callDistort()
  // })

  return (
    <Layout>
      <SEO title='Work' description='' />
      <Container>
        <Con>
          <LabSub>
            The Lab is a place where creativity and experimentation can thrive.
            No rules, no parameters, nothing but full freedom. The design world
            shows no mercy, so upgrading my skills is a priority.
          </LabSub>
        </Con>
        <WorkFeed>
          {data.allMdx.edges.map(edge => (
            <Work
              key={edge.node.id}
              title={edge.node.frontmatter.title}
              image={edge.node.frontmatter.image.childImageSharp.fluid.src}
              slug={`${pathPrefix}${edge.node.fields.slug}`}
              category={edge.node.frontmatter.categories}
            />
          ))}
        </WorkFeed>
        <PagePagination
          currentPage={pageContext.currentPage}
          totalCount={data.allMdx.totalCount}
          pathPrefix='/works/'
        />
      </Container>
    </Layout>
  )
}

export default WorkList

export const PageQuery = graphql`
  query WorkList($skip: Int! = 0) {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      limit: 10
      skip: $skip
      filter: {
        frontmatter: { published: { eq: true } }
        fields: { collection: { eq: "work" } }
      }
    ) {
      totalCount
      edges {
        node {
          id
          fields {
            collection
            slug
          }
          frontmatter {
            desc
            title
            date(formatString: "MMM DD YYYY")
            categories
            image {
              childImageSharp {
                fluid(maxWidth: 1200, quality: 85) {
                  ...GatsbyImageSharpFluid
                  src
                }
              }
            }
          }
        }
      }
    }
  }
`

WorkList.propTypes = {
  pageContext: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
  data: PropTypes.shape({
    allMdx: PropTypes.shape({
      totalCount: PropTypes.string.isRequired,
      edges: PropTypes.array.isRequired,
    }).isRequired,
  }).isRequired,
}

export const Con = styled.div`
  padding: 10%;
`
export const LabSub = styled.h5`
  padding-top: 20px;
  line-height: 28px;
  max-width: 580px;
  font-size: calc(10px + (36 - 14) * ((100vw - 600px) / (4000 - 300)));
  &::before {
    display: block;
    content: '';
    width: 26px;
    height: 1px;
    background-color: black;
    margin-bottom: 10px;
  }
`

export const WorkFeed = styled.div`
  padding: 0 10%;
  display: flex;
  flex-direction: column;
`
