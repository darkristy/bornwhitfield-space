import React from 'react'
import SEO from '../components/seo'
import Layout from '../components/layout'

const Collective = () => {
  return (
    <Layout>
      <SEO
        title='The Collective'
        description='Just a place to store my photos.'
      />
    </Layout>
  )
}

export default Collective
