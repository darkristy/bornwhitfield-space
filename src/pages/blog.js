import React, { useEffect, useRef, useState } from 'react'

import styled from 'styled-components'
import { css } from 'emotion'
import gsap from 'gsap'
import SEO from '../components/seo'
import Layout from '../components/layout'

import { Container } from '../utils/style'
import Featured from '../components/blog/featured'
import Lastest from '../components/blog/latest'
import BlogCTA from '../components/blog/cta'
import MainFooter from '../components/footercomponent/footer'

const BlogPostList = () => {
  let cardRef = useRef(null)
  let feedDiv = useRef(null)
  let contentRef = useRef(null)

  const [t1] = useState(gsap.timeline({ delay: 0.01 }))
  const windowGlobal = typeof window !== 'undefined' && window
  useEffect(() => {
    t1.from(feedDiv, 0.2, {
      autoAlpha: 0,
    }).from([cardRef], 0.8, {
      opacity: 0,
      delay: 0.1,
      y: -24,
      ease: 'power3.out',
    })

    // const content = contentRef
    // let currentPos = windowGlobal.pageYOffset

    // const callDistort = function() {
    //   const newPos = windowGlobal.pageYOffset
    //   const diff = newPos - currentPos
    //   const speed = diff * 0.15

    //   content.style.transform = 'skewY(' + speed + 'deg)'
    //   currentPos = newPos
    //   requestAnimationFrame(callDistort)
    // }
    // callDistort()
  }, [t1, cardRef, feedDiv])

  return (
    <Layout>
      <SEO
        title='The Blog'
        description='Just a place where I can spill my thoughts.'
      />
      <Container ref={el => (contentRef = el)}>
        <div
          //@ts-ignore
          ref={el => (feedDiv = el)}
          className={css`
            visibility: hidden;
          `}
        >
          <Div
            //@ts-ignore
            ref={el => (cardRef = el)}
          >
            <Featured />
            <Lastest />
            <BlogCTA />
          </Div>
        </div>

        <div style={{ padding: '0 10%' }}>
          <MainFooter />
        </div>
      </Container>
    </Layout>
  )
}

export default BlogPostList

const Div = styled.div``
