import React, { useEffect, useRef, useState } from 'react'
import { graphql } from 'gatsby'

import styled from 'styled-components'
import PropTypes from 'prop-types'

import { css } from 'emotion'
import gsap from 'gsap'

import PagePagination from '../components/pagination/page-pagination'

import Pill from '../components/blog/pills'
import SEO from '../components/seo'
import Layout from '../components/layout'

import { Container } from '../utils/style'
import media from '../utils/media'
import BackgroundImage from 'gatsby-background-image'
import AniLink from 'gatsby-plugin-transition-link/AniLink'

const BlogPostList = ({ data, pageContext }) => {
  let cardRef = useRef(null)
  // let arrowRef = useRef(null)
  let feedDiv = useRef(null)

  const pathPrefix = `/post/`

  const [t1] = useState(gsap.timeline({ delay: 0.01 }))
  let workRef = useRef(null)
  const windowGlobal = typeof window !== 'undefined' && window

  useEffect(() => {
    t1.from(feedDiv, 0.2, {
      autoAlpha: 0,
    }).from([cardRef], 0.8, {
      opacity: 0,
      delay: 2,
      y: -24,
      ease: 'power3.out',
    })
    // const content = workRef
    // let currentPos = windowGlobal.pageYOffset
    // const callDistort = function() {
    //   const newPos = windowGlobal.pageYOffset
    //   const diff = newPos - currentPos
    //   const speed = diff * 0.2
    //   content.style.transform = 'skewY(' + speed + 'deg)'
    //   currentPos = newPos
    //   requestAnimationFrame(callDistort)
    // }
    // callDistort()
  }, [t1, cardRef, feedDiv])
  return (
    <Layout>
      <SEO
        title='The Blog'
        description='Just a place where I can spill my thoughts.'
      />
      <Container>
        <div
          ref={el => (feedDiv = el)}
          className={css`
            visibility: hidden;
          `}
        >
          <Div ref={el => (cardRef = el)}>
            <Feed>
              {data.allMdx.edges.map(edge => (
                <Tile key={edge.node.id}>
                  <TileImage>
                    <BackgroundImage
                      fluid={edge.node.frontmatter.image.childImageSharp.fluid}
                      className='image'
                    />
                  </TileImage>
                  <TileContent>
                    <Pill categories={edge.node.frontmatter.categories} />
                    <AniLink
                      cover
                      direction='down'
                      bg='black'
                      to={`${pathPrefix}${edge.node.fields.slug}`}
                    >
                      <TileTitle>{edge.node.frontmatter.title}</TileTitle>
                    </AniLink>

                    <TileExcerpt>{edge.node.excerpt}</TileExcerpt>
                  </TileContent>
                </Tile>
              ))}
            </Feed>

            <PagePagination
              currentPage={pageContext.currentPage}
              totalCount={data.allMdx.totalCount}
              pathPrefix='/archive/'
            />
          </Div>
        </div>
      </Container>
    </Layout>
  )
}

export default BlogPostList

BlogPostList.propTypes = {
  pageContext: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
  data: PropTypes.shape({
    allMdx: PropTypes.shape({
      totalCount: PropTypes.string.isRequired,
      edges: PropTypes.array.isRequired,
    }).isRequired,
  }).isRequired,
}

export const PageQuery = graphql`
  query blogPostsList($skip: Int! = 0) {
    allMdx(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: {
        frontmatter: { published: { eq: true } }
        fields: { collection: { eq: "post" } }
      }
      limit: 10
      skip: $skip
    ) {
      totalCount
      edges {
        node {
          id
          excerpt
          fields {
            slug
            collection
          }
          frontmatter {
            title
            date(formatString: "MMM DD YYYY")
            categories
            image {
              childImageSharp {
                fluid(maxWidth: 1200, quality: 85) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
const Div = styled.div``

const Feed = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  padding: 100px 10%;
`
const Tile = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-auto-rows: auto;
  padding: 5%;
  margin-bottom: 48px;
  transition: 0.3s all ease-in;
  grid-column-start: span 12;
  ${media.bigDesktop`grid-template-columns: 1fr; padding: 0;`}
  ${media.desktop` grid-auto-rows: 330px;`}
  ${media.tablet` grid-auto-rows: 280px; margin-bottom: 28px;`}
  
`
const TileImage = styled.div`
  .image {
    height: 100%;
  }
  ${media.bigDesktop`display: none;`};
`
const TileContent = styled.div`
  padding: 1rem 3rem;
  grid-column: 2;
  ${media.bigDesktop`grid-column: 1; padding: 0px;`}
`

const TileTitle = styled.h2`
  text-transform: uppercase;
  max-width: 800px;
  font-size: 64px;
  line-height: normal;
  transition: ease 0.4s;
  padding-bottom: 40px;
  cursor: pointer;
  ${media.bigDesktop`width:auto;`}
  ${media.desktop`font-size:56px; `}
  ${media.tablet`font-size:48px;`}
  ${media.thone`font-size:28px; padding-bottom: 20px;`}
`
const TileExcerpt = styled.p`
  width: 643px;
  ${media.tablet`max-width:510px; width:100%`}
`
