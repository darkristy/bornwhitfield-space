import React from 'react'
import Layout from '../components/layout'
import Bio from '../components/aboutpage/bio'
import SEO from '../components/seo'
import CV from '../components/aboutpage/cv'
import Footer from '../components/footercomponent/footer'

const About = () => (
  <Layout>
    <SEO title='About' description='The about section of the site.' />
    <Bio />
    <CV />
    <div style={{ padding: '0 10%' }}>
      <Footer />
    </div>
  </Layout>
)

export default About
