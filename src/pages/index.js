import React from 'react'
import Layout from '../components/layout'
import Hero from '../components/homepage/hero'
import SEO from '../components/seo'
import Slider from '../components/homepage/slider'
import Dailogue from '../components/homepage/dialogue'
import MainFooter from '../components/footercomponent/footer'
import HeroCTA from '../components/homepage/get-in-touch'
import TextReel from '../components/homepage/textreel'
import TextStack from '../components/elements/text-stack'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title='Home' description='Welcome Home.' />
      <Hero />
      <Dailogue />
      {/* <TextStack text='what do i do' /> */}
      <TextReel text='currentwork' />
      <Slider />
      <HeroCTA />
      <div style={{ padding: '0 10%' }}>
        <MainFooter />
      </div>
    </Layout>
  )
}

export default IndexPage
