import React, { useEffect, useState, useContext } from 'react'

import Context from '../../context/context'

import { Cursor } from '../../utils/global-styles'

const CustomCursor = () => {
  const { state } = useContext(Context)
  const [mousePosition, setMousePosition] = useState({
    x: 400,
    y: 400,
  })

  const onMouseMove = event => {
    const { pageX: x, pageY: y } = event
    setMousePosition({ x, y })
  }

  useEffect(() => {
    document.addEventListener('mousemove', onMouseMove)
    return () => {
      document.removeEventListener('mousemove', onMouseMove)
    }
  }, [])
  return (
    <>
      <Cursor
        animate={{
          left: `${mousePosition.x}px`,
          top: `${mousePosition.y}px`,
          transition: {
            duration: 0.12,
          },
        }}
        className={`${!!state.cursorType ? 'hovered' : ''} ${state.cursorType}
        `}
        // style={{
        //   left: `${mousePosition.x}px`,
        //   top: `${mousePosition.y}px`,
        // }}
      />
    </>
  )
}

export default CustomCursor
