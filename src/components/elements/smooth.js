import React, { useEffect } from 'react'
import PropTypes from 'prop-types'

const Smoooth = ({ children }) => {
  const windowGlobal = typeof window !== 'undefined' && window

  useEffect(() => {
    const body = document.body,
      scrollWrap = document.getElementsByClassName('smooth-scroll-wrapper')[0],
      height = scrollWrap.getBoundingClientRect().height,
      speed = 0.08

    let offset = 0

    body.style.height = Math.floor(height) + 'px'

    function smoothScroll() {
      offset += (windowGlobal.pageYOffset - offset) * speed

      let scroll = 'translateY(-' + offset + 'px) translateZ(0)'
      scrollWrap.style.transform = scroll

      requestAnimationFrame(smoothScroll)
    }
    smoothScroll()
  })
  return <>{children}</>
}

Smoooth.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Smoooth
