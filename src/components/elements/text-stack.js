import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import media from '../../utils/media'

const TextStack = ({ text }) => {
  const n = 5
  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
    rootMargin: '-300px',
  })
  const animation = useAnimation()

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])

  return (
    <StackWrapper
      ref={ref}
      animate={animation}
      initial='hidden'
      variants={{
        visible: {
          opacity: 1,
          y: 0,
          transition: {
            delay: 0.2,
            duration: 0.5,
            ease: [0.6, 0.05, -0.01, 0.9],
          },
        },
        hidden: {
          opacity: 0,
          y: 83,
        },
      }}
    >
      <ul>
        {[...Array(n)].map((e, i) => (
          <li key={i}>
            <h1>{text}</h1>
          </li>
        ))}
      </ul>
    </StackWrapper>
  )
}

TextStack.propTypes = {
  text: PropTypes.string,
}

export default TextStack

const StackWrapper = styled(motion.div)`
  padding: 0 10%;
  h1 {
    font-size: 144px;
    text-transform: uppercase;
    -webkit-text-fill-color: transparent;
    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: ${props => props.theme.tietary};
  }

  li {
    overflow: hidden;
    &:nth-of-type(1),
    &:nth-of-type(2),
    &:nth-of-type(3) {
      h1 {
      }
    }

    &:nth-of-type(1) {
      height: 40px;
    }
    &:nth-of-type(2) {
      height: 60px;
      margin-top: -20px;
    }
    &:nth-of-type(3) {
      margin-top: -20px;

      h1 {
        -webkit-text-fill-color: ${props => props.theme.tietary};
      }
    }
    &:nth-of-type(4) {
      height: 60px;
      margin-top: -39px;
      transform: translate3d(0, -40%, 0);
    }
    &:nth-of-type(5) {
      height: 40px;
      margin-top: -28px;
      transform: translate3d(0, -40%, 0);
    }
  }
`
