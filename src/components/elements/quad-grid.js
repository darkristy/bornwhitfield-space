import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import media from '../../utils/media'

const QuadGrid = ({ children, gap }) => {
  const QupleGrid = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-auto-rows: repeat(2, 1fr);
    grid-gap: ${gap};
    ${media.bigDesktop` grid-gap: 80px;`}
    ${media.tablet` grid-gap: 100px;`}
    ${media.thone`grid-template-columns: 1fr; grid-gap: 20px;`}
  `
  return <QupleGrid>{children}</QupleGrid>
}

QuadGrid.propTypes = {
  children: PropTypes.node.isRequired,
  gap: PropTypes.string.isRequired,
}

export default QuadGrid
