import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import media from '../../utils/media'

const Caption = ({ text }) => {
  return (
    <CaptionText>
      <em>{text}</em>
    </CaptionText>
  )
}

Caption.propTypes = {
  text: PropTypes.string.isRequired,
}

export default Caption

const CaptionText = styled.div`
  em {
    font-size: 18px;
    color: gray;
    margin-top: 20px;
    margin-top: 140px;

    ${media.thone`font-size: 11px; margin-bottom: 84px;`}
  }
`
