import React from 'react'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import PropTypes from 'prop-types'
import styled from 'styled-components'

export const MainButton = ({ path, text }) => {
  return <div></div>
}

MainButton.propTypes = {
  path: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}
