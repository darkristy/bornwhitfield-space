import React, { useRef, useEffect } from 'react'
import gsap, { Expo } from 'gsap'
import styled from 'styled-components'

const Overlay = () => {
  let firstSlab = useRef(null)
  let secondSlab = useRef(null)
  let thirdSlab = useRef(null)
  let disableOverlay = useRef(null)

  useEffect(() => {
    gsap.to(firstSlab, 1.2, {
      delay: 0.5,
      top: '-100%',
      ease: Expo.easeInOut,
    })
    gsap.to(secondSlab, 1.2, {
      delay: 0.7,
      top: '-100%',
      ease: Expo.easeInOut,
    })
    gsap.to(thirdSlab, 1.2, {
      delay: 0.9,
      top: '-100%',
      ease: Expo.easeInOut,
    })
    gsap.to(disableOverlay, 1.2, { css: { display: 'none' }, delay: 1 })
  })

  return (
    <OverlayContainer ref={el => (disableOverlay = el)}>
      <div className='slab' ref={el => (firstSlab = el)} />
      <div className='slab' ref={el => (secondSlab = el)} />
      <div className='slab' ref={el => (thirdSlab = el)} />
    </OverlayContainer>
  )
}

export default Overlay

const OverlayContainer = styled.div`
  position: absolute;
  display: flex;
  grid-template-columns: repeat(3, 1fr);
  width: 100vw;
  height: 100vh;
  top: 0;
  .slab {
    position: relative;
    width: 100%;
    height: 100%;
    z-index: 99;
    background: ${props => props.theme.tietary};
  }
`
