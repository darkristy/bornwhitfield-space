import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import media from '../../utils/media'

const DoubleGrid = ({ children, columns, gap }) => {
  const TupleGrid = styled.div`
    display: grid;
    grid-template-columns: ${columns};
    grid-auto-rows: auto;
    grid-gap: ${gap};
    ${media.bigDesktop` grid-gap: 80px;`}
    ${media.tablet` grid-gap: 100px;`}
    ${media.thone`grid-template-columns: 1fr; grid-gap: 20px;`}
  `
  return <TupleGrid>{children}</TupleGrid>
}

DoubleGrid.propTypes = {
  children: PropTypes.node.isRequired,
  columns: PropTypes.string.isRequired,
  gap: PropTypes.string.isRequired,
}

export default DoubleGrid
