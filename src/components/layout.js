import React, { useReducer, useEffect, useState } from 'react'
import { GlobalStyles } from '../utils/global-styles'
import Header from './header'
import PropTypes from 'prop-types'
import { ThemeProvider } from 'styled-components'
import reducer from '../context/reducer'
import Context from '../context/context'
import CustomCursor from '../components/elements/custom-cursor'
import * as pallete from '../utils/variables'

const Layout = ({ children }) => {
  const windowGlobal = () => typeof window !== 'undefined'

  const [state, dispatch] = useReducer(reducer, {
    currentTheme:
      windowGlobal() && window.localStorage.getItem('theme') == null
        ? 'dark'
        : windowGlobal() && window.localStorage.getItem('theme'),
    cursorType: false,
    cursorStyles: [
      'pointer',
      'hovered',
      'locked',
      'toggle',
      'hide',
      'w-button',
      'b-button',
    ],
  })

  useEffect(() => {
    windowGlobal() && window.localStorage.setItem('theme', state.currentTheme)
  }, [state.currentTheme])

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  const [togglePosition, setTogglePosition] = useState({ x: 0, y: 0 })

  const darkTheme = {
    text: `${pallete.DARK_TIETARY_COLOR}`,
    background: `${pallete.DARK_PRIMARY_COLOR}`,
    primary: `${pallete.DARK_PRIMARY_COLOR}`,
    secondary: `${pallete.DARK_SECONDARY_COLOR}`,
    tietary: `${pallete.DARK_TIETARY_COLOR}`,
    accent: `${pallete.CORAL}`,
    left: `${togglePosition.x}px`,
    top: `${togglePosition.y}px`,
  }

  const lightTheme = {
    text: `${pallete.LIGHT_TIETARY_COLOR}`,
    background: `${pallete.LIGHT_PRIMARY_COLOR}`,
    primary: `${pallete.LIGHT_PRIMARY_COLOR}`,
    secondary: `${pallete.LIGHT_SECONDARY_COLOR}`,
    tietary: `${pallete.LIGHT_TIETARY_COLOR}`,
    accent: `${pallete.CORAL}`,
    left: `${togglePosition.x}px`,
    top: `${togglePosition.y}px`,
  }

  return (
    <Context.Provider value={{ state, dispatch }}>
      <ThemeProvider
        theme={state.currentTheme === 'dark' ? darkTheme : lightTheme}
      >
        <GlobalStyles />

        <CustomCursor />
        <Header
          onCursor={onCursor}
          togglePosition={togglePosition}
          setTogglePosition={setTogglePosition}
        />
        <main>{children}</main>
      </ThemeProvider>
    </Context.Provider>
  )
}

export default Layout

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}
