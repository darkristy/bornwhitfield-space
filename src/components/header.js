import React from 'react'
import styled from 'styled-components'
import Nav from './navigation/nav'
import PropTypes from 'prop-types'

const Header = ({ onCursor, togglePosition, setTogglePosition }) => {
  return (
    <>
      <HeaderWrapper>
        <Nav
          onCursor={onCursor}
          togglePosition={togglePosition}
          setTogglePosition={setTogglePosition}
        />
      </HeaderWrapper>
    </>
  )
}

export default Header

const HeaderWrapper = styled.header`
  background: ${props => props.theme.primary};
  position: relative;
  width: 100%;
  z-index: 9;
`
Header.propTypes = {
  onCursor: PropTypes.any,
  togglePosition: PropTypes.any,
  setTogglePosition: PropTypes.any,
}
