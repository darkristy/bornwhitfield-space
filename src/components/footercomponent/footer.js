import React, { useContext, useEffect } from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import Social from './social'
import Context from '../../context/context'
import { useInView } from 'react-intersection-observer'
import { useAnimation, motion } from 'framer-motion'

const MainFooter = () => {
  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  const animation = useAnimation()
  const [footerRef, inView] = useInView({
    triggerOnce: true,
  })
  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [animation, inView])

  return (
    <Footer
      ref={footerRef}
      animate={animation}
      initial='hidden'
      variants={{
        visible: {
          opacity: 1,
          y: 0,
          transition: { duration: 0.8, ease: [0.6, 0.05, -0.01, 0.9] },
        },
        hidden: { opacity: 0, y: -72 },
      }}
    >
      <FooterRow>
        <Social onCursor={onCursor} />
        <FooterCopyright>
          <h4>© {new Date().getFullYear()} Kahlil Whitfield</h4>
        </FooterCopyright>
      </FooterRow>
    </Footer>
  )
}

export default MainFooter

export const Footer = styled(motion.footer)`
  /* max-width: 1280px; */
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 80px;
  margin-top: 80px;
`
export const FooterRow = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  ${media.desktop`
  grid-template-columns: none;
  grid-template-rows: repeat(2, 1fr);`};
`

export const FooterCopyright = styled.div`
  grid-column: 2;
  text-align: right;
  ${media.desktop`grid-row: 2; grid-column: 1; text-align: left;padding: 6px 0;`};
`
