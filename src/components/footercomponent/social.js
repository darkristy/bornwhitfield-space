import React from 'react'
import PLUGS from '../../constants/plugs'
import { SocialLink } from '../../utils/style'

import PropTypes from 'prop-types'

const Social = ({ onCursor }) => {
  return (
    <SocialLink>
      <div>
        <h4>
          {PLUGS.map((s, idx) => (
            <React.Fragment key={s.name}>
              <a
                href={s.href}
                className={s.className}
                target='_blank'
                rel='noopener noreferrer'
                onMouseEnter={() => onCursor('hovered')}
                onMouseLeave={onCursor}
              >
                {s.name}
              </a>
              {idx === PLUGS.length - 1 ? '' : <span> / </span>}
            </React.Fragment>
          ))}
        </h4>
      </div>
    </SocialLink>
  )
}

export default Social

Social.propTypes = {
  onCursor: PropTypes.any,
}
