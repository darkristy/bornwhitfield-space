import React from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import Social from './social'

const BlogFooter = () => {
  return (
    <Footer>
      <FooterRow>
        <Social />
        <FooterCopyright>
          <h4>© {new Date().getFullYear()} Kahlil Whitfield</h4>
        </FooterCopyright>
      </FooterRow>
    </Footer>
  )
}

export default BlogFooter

export const Footer = styled.footer`
  padding: 0 10%;
  max-width: 1280px;
  margin-left: auto;
  margin-right: auto;
  margin-bottom: 80px;
  margin-top: 80px;
`
export const FooterRow = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  ${media.desktop`
  grid-template-columns: none;
  grid-template-rows: repeat(2, 1fr);`};
`

export const FooterCopyright = styled.div`
  grid-column: 2;
  text-align: right;
  ${media.desktop`grid-row: 2; grid-column: 1; text-align: left;padding: 6px 0;`};
`
