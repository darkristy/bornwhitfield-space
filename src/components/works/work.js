import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import PropTypes from 'prop-types'

import { motion } from 'framer-motion'
import useGetCursorPosition from '../../hooks/getCursorPosition'
import { Container } from '../../utils/style'
import Context from '../../context/context'

const Work = ({ title, slug, category, image, key }) => {
  const [revealImage, setRevealImage] = useState({
    show: false,
    image: image,
    key: '0',
  })

  const mousePosition = useGetCursorPosition()

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  return (
    <>
      <ImageWrapper>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{
            opacity: revealImage.show ? 1 : 0,
            x: `${mousePosition.x}`,
            y: `${mousePosition.y}`,
            transition: {
              duration: 0.6,
            },
          }}
          transition={{ ease: 'easeInOut', duration: 0.4 }}
          className='image'
          style={{
            backgroundImage: `url(${image})`,
            left: `${mousePosition.x - 800}px`,
            top: `${mousePosition.y - 900}px`,
          }}
        />
      </ImageWrapper>
      <Container>
        <WorkWrapper className='section'>
          <WorkInfo>
            <WorkCategory>{category}</WorkCategory>
            <WorkTitle>
              <AniLink cover direction='down' bg='black' to={slug}>
                <motion.span
                  data-text={title}
                  onHoverStart={() =>
                    setRevealImage({
                      show: true,
                      image: image,
                      key: key,
                    })
                  }
                  onHoverEnd={() =>
                    setRevealImage({
                      show: false,
                      image: image,
                      key: key,
                    })
                  }
                  onMouseEnter={() => onCursor('hide')}
                  onMouseLeave={onCursor}
                >
                  {title}
                </motion.span>
              </AniLink>
            </WorkTitle>
          </WorkInfo>
        </WorkWrapper>
      </Container>
    </>
  )
}

export default Work

Work.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  key: PropTypes.any.isRequired,
  slug: PropTypes.string.isRequired,
  category: PropTypes.any.isRequired,
}

export const WorkWrapper = styled.div`
  padding: 40px 0;
  margin: 60px 0;
`
const WorkInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;

  transition: transform 0.6s;
`

const WorkCategory = styled.h4`
  padding-bottom: 20px;
  text-transform: uppercase;
`
// const WorkDate = styled.p`
//   margin-left: 40px;
// `
const WorkTitle = styled.h1`
  line-height: 88px;

  transition: all 0.4s ease;
  font-size: 96px;
  -webkit-text-fill-color: transparent;
  -webkit-text-stroke-width: 0.8px;
  -webkit-text-stroke-color: ${props => props.theme.tietary};
  white-space: nowrap;
  span {
    position: relative;

    &:before {
      width: 0;
      color: ${props => props.theme.tietary};
      -webkit-text-fill-color: ${props => props.theme.tietary};
      overflow: hidden;
      position: absolute;
      content: attr(data-text);
      transition: all 1s cubic-bezier(0.84, 0, 0.08, 0.99);
      white-space: nowrap;
    }
    &:hover {
      &:before {
        width: 100%;
      }
    }
  }
`
const ImageWrapper = styled.div`
  width: 100%;
  height: 100vh;
  position: absolute;
  display: flex;
  justify-content: center;
  z-index: -1;
  transition: all 29s ease 0s;
  filter: brightness(60%);
  .image {
    position: absolute;
    width: 100%;
    max-width: 1200px;
    height: 700px;
    background-size: cover;
    overflow: hidden;
  }
`
