import React, { useRef, useState, useEffect } from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'
import PropTypes from 'prop-types'

import { graphql, useStaticQuery } from 'gatsby'
import gsap, { Power3 } from 'gsap'
import { useInView } from 'react-intersection-observer'
import Caption from '../elements/caption'

const Image = ({ imgName, text, isCaption }) => {
  let imgRef = useRef(null)
  let imgHolder = useRef(null)
  let captionHolder = useRef(null)
  let over = useRef(null)

  const [t1] = useState(gsap.timeline({ delay: 0.2 }))

  /*eslint-disable */
  const [ref, inView, entry] = useInView({
    threshold: 0,
    triggerOnce: true,
  })

  const data = useStaticQuery(graphql`
    query Images {
      allImageSharp {
        edges {
          node {
            fluid(maxWidth: 1600, quality: 100) {
              ...GatsbyImageSharpFluid
              originalName
            }
          }
        }
      }
    }
  `)

  useEffect(() => {
    if (inView === true) {
      t1.to(imgHolder, 0, { css: { visibility: 'visible' }, delay: 0.2 })
        .to(over, 1.4, { width: '0%', ease: Power3.easeInOut })
        .from(imgRef, 1.4, {
          skewY: -6,
          ease: Power3.easeInOut,
          delay: -1.6,
        })
      // .from(imgRef, 1.4, {
      //   scale: 1.6,
      //   ease: Power3.easeInOut,
      //   delay: -1.6,
      // })
      if (isCaption === false) {
        return null
      } else {
        t1.to(captionHolder, 0, { css: { visibility: 'visible' }, delay: -0.2 })
      }
    }
  }, [imgHolder, over, imgRef, t1, inView, captionHolder])

  const image = data.allImageSharp.edges.find(
    edge => edge.node.fluid.originalName === imgName
  )
  if (!image) {
    return null
  }
  return (
    <div ref={ref} inView={inView}>
      {isCaption === false ? (
        <div> </div>
      ) : (
        <CaptionHolder ref={el => (captionHolder = el)}>
          <Caption text={text} />
        </CaptionHolder>
      )}

      <ImageHolder ref={el => (imgHolder = el)}>
        <ImageOver ref={el => (over = el)} />
        <div ref={el => (imgRef = el)}>
          <Img fluid={image.node.fluid} />
        </div>
      </ImageHolder>
    </div>
  )
}

export default Image

Image.propTypes = {
  imgName: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  isCaption: PropTypes.any,
}

const ImageHolder = styled.div`
  position: relative;
  visibility: hidden;
  overflow: hidden;
`
const ImageOver = styled.div`
  width: 100%;
  height: 100%;
  background: ${props => props.theme.primary};
  position: absolute;
  z-index: 2;
`
const CaptionHolder = styled.div`
  visibility: hidden;
  overflow: hidden;
`
