import React from 'react'
import styled from 'styled-components'
import { IntroTitle } from '../homepage/hero'
import BackgroundImage from 'gatsby-background-image'
import media from '../../utils/media'
import PropTypes from 'prop-types'

const Intro = ({ image, title, desc, date, role, categories }) => {
  return (
    <IntroContainer>
      <IntroImage>
        <BackgroundImage className='image' fluid={image} />
      </IntroImage>
      <Content>
        <IntroTitle>
          <h1>{title}</h1>
          <h1>{title}</h1>
        </IntroTitle>
        <IntroInfoBox>
          <IntroProjectInfo>
            <h1>Project Info</h1>
            <p>{date}</p>
            {categories.map(category => (
              <div key={category}>
                <p>{category}</p>
              </div>
            ))}
            <p>{role}</p>
          </IntroProjectInfo>
          <IntroDescription>
            <p>{desc}</p>
          </IntroDescription>
        </IntroInfoBox>
      </Content>
    </IntroContainer>
  )
}

export default Intro

Intro.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  categories: PropTypes.array.isRequired,
}

const IntroContainer = styled.div`
  p {
    font-size: 18px;
    line-height: 32px;
    font-weight: 300;
    text-transform: initial;
    @media screen and (max-width: 767px) {
      font-size: 16px;
    }
  }
  margin-bottom: 248px;
  ${media.desktop` margin-bottom: 124px;`}
`

const IntroInfoBox = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;
  grid-auto-rows: auto;
  h1 {
    font-size: 24px;
    text-transform: uppercase;
    padding-bottom: 10px;
  }

  p {
    ${media.desktop`font-size: 12px;`}
  }
  ${media.desktop` grid-template-columns: 1fr; padding: 0 ;`}
`
const IntroProjectInfo = styled.div`
  p {
    font-size: 14px;
    text-transform: uppercase;
  }
`
const IntroDescription = styled.div`
  ${media.desktop`padding-top: 48px;`}
`
const IntroImage = styled.div`
  margin-top: 80px;
  height: 680px;
  ${media.thone`
    height:320px;`}
  .image {
    height: 100%;
  }
`
const Content = styled.div`
  margin-top: 140px;
  padding: 0 20%;
  ${media.bigDesktop`padding: 0 10%`}
  ${media.thone`margin-top: 84px;`}
`
