import React, { useEffect, useContext } from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import styled from 'styled-components'
import media from '../../utils/media'
import LatestEntries from './entry'
import PropTypes from 'prop-types'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import Context from '../../context/context'

const Latest = ({ data }) => {
  const entries = data.allMdx.edges

  const [ref, inView] = useInView({
    threshold: 0.2,
    triggerOnce: true,
    rootMargin: '-300px',
  })
  const animation = useAnimation()

  const header = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.4,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 34,
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  return (
    <div ref={ref}>
      <LastestHeading animate={animation} initial='hidden' variants={header}>
        <h2>Latest Blog Posts</h2>
        <h2>Latest Blog Posts</h2>
      </LastestHeading>

      <LatestWrapper>
        <LatestEntries entries={entries} onCursor={onCursor} />
      </LatestWrapper>
    </div>
  )
}

export default props => {
  const data = useStaticQuery(graphql`
    query LatestQuery {
      allMdx(
        skip: 1
        limit: 4
        sort: { fields: [frontmatter___date], order: DESC }
        filter: {
          frontmatter: { published: { eq: true } }
          fields: { collection: { eq: "post" } }
        }
      ) {
        edges {
          node {
            id
            excerpt
            fields {
              slug
            }
            frontmatter {
              title
              categories
              date(formatString: "DD MMMM, YYYY")
              image {
                childImageSharp {
                  fluid {
                    src
                  }
                }
              }
            }
          }
        }
      }
    }
  `)
  return <Latest data={data} {...props} />
}

Latest.propTypes = {
  pageContext: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
  data: PropTypes.shape({
    allMdx: PropTypes.shape({
      edges: PropTypes.array.isRequired,
    }).isRequired,
  }).isRequired,
}

const LastestHeading = styled(motion.div)`
  padding-top: 190px;
  padding-bottom: 80px;
  h2 {
    padding: 0 10%;
    font-size: 64px;
    transition: ease 0.4s;
    text-transform: uppercase;
    ${media.tablet`font-size: 34px; `}

    &:nth-child(2) {
      margin-top: -62px;
      ${media.tablet` margin-top: -18px; `}
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 0.6px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
  }
`

const LatestWrapper = styled.div`
  padding: 50px 10%;
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-auto-rows: 248px;
  max-width: 1020px;
  /* max-width: 920px; */
  width: 100%;
  grid-row-gap: 121px;
  grid-column-gap: 130px;
  ${media.bigDesktop` grid-column-gap: 80px;`}
  ${media.tablet` grid-column-gap: 0px;`}
`

// const Category = styled.h5`
//   padding-bottom: 8px;
//   color: gray;
// `
