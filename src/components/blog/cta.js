import React, { useEffect, useContext } from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import { useAnimation, motion } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import Context from '../../context/context'

const BlogCTA = () => {
  const path = `/archive`

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
    rootMargin: '-300px',
  })
  const animation = useAnimation()

  const heading = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.4,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 34,
    },
  }

  const copy = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.6,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 34,
    },
  }

  const button = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.6,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 34,
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])

  return (
    <CTAContainer ref={ref}>
      <CTAHeading animate={animation} initial='hidden' variants={heading}>
        <h1>Creating Content Daily is a must</h1>
        <h1>Creating Content Daily is a must</h1>
      </CTAHeading>
      <CTAInfoBox>
        <CTACopy animate={animation} initial='hidden' variants={copy}>
          <p>
            If you like what I am putting out I encourage you to view more,
            either on the site or my social media below.
          </p>
        </CTACopy>
        <AniLink cover direction='down' bg='black' to={path}>
          <CTAButton
            animate={animation}
            initial='hidden'
            variants={button}
            onMouseEnter={() => onCursor('w-button')}
            onMouseLeave={onCursor}
          >
            <p>view more</p>
          </CTAButton>
        </AniLink>
      </CTAInfoBox>
    </CTAContainer>
  )
}

export default BlogCTA

const CTAContainer = styled(motion.div)`
  padding: 220px 10%;
`

const CTAInfoBox = styled(motion.div)`
  display: flex;
  margin-left: 38%;
  ${media.bigDesktop`margin-left: 0;`}
  ${media.tablet`padding-left: 0; display: block;`}
`
const CTACopy = styled(motion.div)`
  p {
    width: 395px;
    font-size: 14px;
    padding-bottom: 27px;
  }
  margin-right: 96px;
`
const CTAButton = styled(motion.div)`
  background: none;
  border: 1px solid ${props => props.theme.tietary};
  p {
    color: ${props => props.theme.tietary};
    font-size: 14px;
    text-transform: uppercase;
  }
  height: 50px;
  padding: 17px 20px;
  width: 100%;
  max-width: 210px;
  text-align: center;

  transition: all 0.4s ease;
  &:hover {
    transition: all 0.4s ease;
    background: ${props => props.theme.tietary};
    p {
      color: ${props => props.theme.primary};
    }
    border: none;
  }
`

const CTAHeading = styled(motion.div)`
  h1 {
    font-size: 100px;
    ${media.desktop`font-size: 64px; max-width: 611px;`}
    ${media.tablet`font-size: 38px;max-width: 411px; padding-bottom: 36px;`};
    &:nth-of-type(2) {
      margin-top: -320px;
      ${media.desktop` margin-top: -240px; `}
      ${media.tablet` margin-top: -118px; `}
      
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 0.6px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
    text-transform: uppercase;
    width: 100%;
    max-width: 811px;
    padding-bottom: 96px;
  }
`
