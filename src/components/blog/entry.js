import React, { useEffect } from 'react'
import styled from 'styled-components'
import { BlogButton } from '../elements/buttons'
import media from '../../utils/media'
import * as pallete from '../../utils/variables'
import PropTypes from 'prop-types'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import AniLink from 'gatsby-plugin-transition-link/AniLink'

const LatestEntries = ({ entries, onCursor }) => {
  const post = entries
  const numbers = [{ num: '01' }, { num: '02' }, { num: '03' }, { num: '04' }]

  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
    rootMargin: '-300px',
  })
  const animation = useAnimation()

  const line = {
    visible: {
      width: '100%',
      transition: {
        delay: 0.2,
        duration: 1,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      width: 0,
    },
  }

  const number = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.2,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 24,
    },
  }

  const title = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.4,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 24,
    },
  }
  const excerpt = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.6,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 24,
    },
  }

  const button = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.8,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 24,
    },
  }

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])

  return (
    <>
      <Entry ref={ref}>
        <EntryInfoContainer>
          <Line animate={animation} initial='hidden' variants={line} />
          <EntryNumber animate={animation} initial='hidden' variants={number}>
            <p>{numbers[0].num}</p>
          </EntryNumber>
          <EntryTitle animate={animation} initial='hidden' variants={title}>
            <h4>{post[0].node.frontmatter.title}</h4>
          </EntryTitle>
          <EntryExcerpt animate={animation} initial='hidden' variants={excerpt}>
            <p>{post[0].node.excerpt}</p>
          </EntryExcerpt>
          <AniLink
            style={{ gridColumnStart: 'span 2' }}
            cover
            direction='down'
            bg='black'
            to={`/post${post[0].node.fields.slug}`}
          >
            <EntryButton
              onMouseEnter={() => onCursor('hovered')}
              onMouseLeave={onCursor}
              animate={animation}
              initial='hidden'
              variants={button}
            >
              <p>View Story</p>
            </EntryButton>
          </AniLink>
        </EntryInfoContainer>
      </Entry>
      <Entry>
        <EntryInfoContainer>
          <Line animate={animation} initial='hidden' variants={line} />
          <EntryNumber animate={animation} initial='hidden' variants={number}>
            <p>{numbers[1].num}</p>
          </EntryNumber>
          <EntryTitle animate={animation} initial='hidden' variants={title}>
            <h4>{post[1].node.frontmatter.title}</h4>
          </EntryTitle>
          <EntryExcerpt animate={animation} initial='hidden' variants={excerpt}>
            <p>{post[1].node.excerpt}</p>
          </EntryExcerpt>
          <AniLink
            style={{ gridColumnStart: 'span 2' }}
            cover
            direction='down'
            bg='black'
            to={`/post${post[1].node.fields.slug}`}
          >
            <EntryButton
              onMouseEnter={() => onCursor('hovered')}
              onMouseLeave={onCursor}
              animate={animation}
              initial='hidden'
              variants={button}
            >
              <p>View Story</p>
            </EntryButton>
          </AniLink>
        </EntryInfoContainer>
      </Entry>
      <Entry>
        <EntryInfoContainer>
          <Line animate={animation} initial='hidden' variants={line} />
          <EntryNumber animate={animation} initial='hidden' variants={number}>
            <p>{numbers[2].num}</p>
          </EntryNumber>
          <EntryTitle animate={animation} initial='hidden' variants={title}>
            <h4>{post[2].node.frontmatter.title}</h4>
          </EntryTitle>
          <EntryExcerpt animate={animation} initial='hidden' variants={excerpt}>
            <p>{post[2].node.excerpt}</p>
          </EntryExcerpt>

          <AniLink
            style={{ gridColumnStart: 'span 2' }}
            cover
            direction='down'
            bg='black'
            to={`/post${post[2].node.fields.slug}`}
          >
            <EntryButton
              onMouseEnter={() => onCursor('hovered')}
              onMouseLeave={onCursor}
              animate={animation}
              initial='hidden'
              variants={button}
            >
              <p>View Story</p>
            </EntryButton>
          </AniLink>
        </EntryInfoContainer>
      </Entry>
      <Entry key={post[3].node.id}>
        <EntryInfoContainer>
          <Line animate={animation} initial='hidden' variants={line} />
          <EntryNumber animate={animation} initial='hidden' variants={number}>
            <p>{numbers[3].num}</p>
          </EntryNumber>
          <EntryTitle animate={animation} initial='hidden' variants={title}>
            <h4>{post[3].node.frontmatter.title}</h4>
          </EntryTitle>
          <EntryExcerpt animate={animation} initial='hidden' variants={excerpt}>
            <p>{post[3].node.excerpt}</p>
          </EntryExcerpt>
          <AniLink
            style={{ gridColumnStart: 'span 2' }}
            cover
            direction='down'
            bg='black'
            to={`/post${post[3].node.fields.slug}`}
          >
            <EntryButton
              onMouseEnter={() => onCursor('hovered')}
              onMouseLeave={onCursor}
              animate={animation}
              initial='hidden'
              variants={button}
            >
              <p>View Story</p>
            </EntryButton>
          </AniLink>
        </EntryInfoContainer>
      </Entry>
    </>
  )
}

export default LatestEntries

LatestEntries.propTypes = {
  entries: PropTypes.any.isRequired,
  onCursor: PropTypes.any,
}

const Entry = styled.div`
  grid-column-start: span 4;
  ${media.tablet` grid-column-start: span 8;`}
`

const EntryInfoContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
`

const Line = styled(motion.div)`
  grid-column-start: span 4;
  background: ${props => props.theme.tietary};
  width: 100%;
  height: 1px;
`
const EntryNumber = styled(motion.div)`
  padding: 20px 0;
  p {
    color: ${props => props.theme.tietary};
    font-size: 14px;
  }
`

const EntryTitle = styled(motion.div)`
  grid-column-start: span 4;
  h4 {
    font-family: ${pallete.PRIMARY_FONT};
    font-size: 32px;
    text-transform: uppercase;
    ${media.desktop`font-size: 28px;`}
  }
`
const EntryExcerpt = styled(motion.div)`
  grid-column-start: span 4;
  padding-top: 20px;
  padding-bottom: 47px;
  p {
    font-size: 14px;
  }
`

export const EntryButton = styled(motion.div)`
  grid-column-start: span 2;
  padding: 7px 0;
  width: 100%;
  background: ${props => props.theme.tietary};
  p {
    color: ${props => props.theme.primary};
    text-transform: uppercase;
    font-size: 14px;
    text-align: center;
  }
  transition: all 0.4s ease;
  &:hover {
    transition: all 0.4s ease;
    background: none;
    p {
      color: ${props => props.theme.tietary};
    }

    border: 1px solid ${props => props.theme.tietary};
  }
`
