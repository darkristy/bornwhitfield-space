import React from 'react'
import styled from 'styled-components'
import * as pallete from '../../utils/variables'
import './pill.css'
import capitalize from '../../utils/capitalize'
import media from '../../utils/media'
import PropTypes from 'prop-types'

function cssSafe(str) {
  return encodeURIComponent(str.toLowerCase()).replace(/%[0-9A-F]{2}/gi, '')
}

const Pill = ({ categories }) => {
  const PillBlock = styled.div`
    display: inline-block;
    justify-content: center;
    font-size: 14px;
    padding: 8px 20px;
    border-radius: 12px;
    font-family: ${pallete.SECONDARY_FONT};
    margin-bottom: 18px;
    margin-top: 10px;
    margin-right: 16px;
    color: ${props => props.theme.primary};
    ${media.thone`font-size:12px; padding: 4px 10px; border-radius: 6px;`}
  `
  return (
    <div>
      {categories.map((category, idx) => (
        <PillBlock key={idx} className={`pill--${cssSafe(category)}`}>
          {capitalize(category)}
        </PillBlock>
      ))}
    </div>
  )
}

export default Pill

Pill.propTypes = {
  categories: PropTypes.any.isRequired,
  theme: PropTypes.any.isRequired,
}
