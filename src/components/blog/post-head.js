import React from 'react'
import styled from 'styled-components'
//Fontawesome
import '@fortawesome/fontawesome-svg-core/styles.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClock, faBook } from '@fortawesome/free-solid-svg-icons'

import media from '../../utils/media'
import PropTypes from 'prop-types'

let theStyle = {
  marginRight: '10px',
}

const PostHead = ({ image, readtime, date, title }) => {
  return (
    <PostHeader>
      <PostHeadWrapper>
        <PostTitle>{title}</PostTitle>

        <PostDataWrapper>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <PostDate>
              <FontAwesomeIcon icon={faClock} style={theStyle} />
              {date}
            </PostDate>
            <PostReadTime>
              <FontAwesomeIcon icon={faBook} style={theStyle} />
              {readtime} min read
            </PostReadTime>
          </div>
        </PostDataWrapper>
      </PostHeadWrapper>
      <PostHero
        style={{
          backgroundImage: `linear-gradient(
                to bottom, 
                rgba(10,10,10,0) 0%,
                rgba(10,10,10,0) 5%,
                rgba(10,10,10,0.4) 100%),url(${image})`,
        }}
      />
    </PostHeader>
  )
}

export default PostHead

PostHead.propTypes = {
  image: PropTypes.any.isRequired,
  readtime: PropTypes.any.isRequired,
  date: PropTypes.any.isRequired,
  title: PropTypes.any.isRequired,
}

const PostTitle = styled.h1`
  margin-top: 20px;
  max-width: 890px;
  margin-bottom: 20px;

  font-size: calc(2.125rem + ((1vw - 6.76px) * 2.4116));
  text-align: center;
  ${media.bigDesktop`text-align:left;
  color: ${props => props.theme.white};
  `}
  @media screen and (max-width: 767px) {
    margin-bottom: 40px;
  }
  text-transform: uppercase;
`
const PostHeader = styled.div`
  padding: 0 10%;
  margin: 0 auto;
  margin-bottom: 100px;
  ${media.bigDesktop`display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  grid-template-areas: "overlap";padding: 0;`}
`
const PostHero = styled.div`
  background-position: top;
  background-size: cover;
  background-repeat: no-repeat;
  height: 580px;
  width: 100%;
  ${media.bigDesktop`grid-area: overlap; z-index: -1; position: relative;`}
`
const PostHeadWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  ${media.bigDesktop`grid-area: overlap;
  align-items: start ;
  padding: 0 10%;
  min-height: 600px;
  margin-top:120px;
  `}
`
const PostDataWrapper = styled.div`
  margin-bottom: 100px;
  font-family: Inconsolata, monospace;
`
const PostDate = styled.div`
  margin-right: 10px;
  color: ${props => props.theme.tietary};
  ${media.bigDesktop` color: ${props => props.theme.white};`}
`
const PostReadTime = styled.div`
  margin-left: 10px;
  color: ${props => props.theme.tietary};
  ${media.bigDesktop` color: ${props => props.theme.white};`}
`
