import React, { useEffect, useContext } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import media from '../../utils/media'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import PropTypes from 'prop-types'
import { motion, useAnimation } from 'framer-motion'
import { useInView } from 'react-intersection-observer'
import Context from '../../context/context'

const Featured = ({ data }) => {
  const { mdx } = data
  const { frontmatter, fields } = mdx
  const { image, title } = frontmatter
  const { slug } = fields
  const imageData = image.childImageSharp.fluid.src

  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
  })
  const animation = useAnimation()

  // const featImage = {
  //   visible: {
  //     width: '100%',
  //     transition: {
  //       delay: 0.6,
  //       duration: 0.8,
  //       ease: [0.6, 0.05, -0.01, 0.9],
  //     },
  //   },
  //   hidden: {
  //     width: 0,
  //   },
  // }

  const wrapper = {
    visible: {
      opacity: 1,
      y: 0,
      transition: {
        delay: 0.9,
        duration: 0.8,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      opacity: 0,
      y: 34,
    },
  }
  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])
  return (
    <FeaturedWrapper
      ref={ref}
      animate={animation}
      initial='hidden'
      variants={wrapper}
    >
      <FeaturedEntry>
        <FeaturedImage
          style={{
            backgroundImage: `url(${imageData})`,
          }}
          // animate={animation}
          // initial='hidden'
          // variants={featImage}
        />

        <FeaturedContent>
          <p>Featured</p>
          <h1>{title}</h1>
          <FeaturedBlogButton path={`/post${slug}`}>
            <p>View Story</p>
          </FeaturedBlogButton>
        </FeaturedContent>
      </FeaturedEntry>
    </FeaturedWrapper>
  )
}

const FeaturedBlogButton = ({ path }) => {
  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  return (
    <AniLink
      style={{ gridColumnStart: 'span 2' }}
      cover
      direction='down'
      bg='black'
      to={path}
    >
      <FeaturedButton
        onMouseEnter={() => onCursor('hovered')}
        onMouseLeave={onCursor}
      >
        <p>View Story</p>
      </FeaturedButton>
    </AniLink>
  )
}

export default props => {
  return (
    <StaticQuery
      query={graphql`
        query FeaturedQuery {
          mdx(
            frontmatter: { published: { eq: true }, featured: { eq: true } }
            fields: { collection: { eq: "post" } }
          ) {
            id
            fields {
              slug
            }
            frontmatter {
              title
              categories
              date(formatString: "DD MMMM, YYYY")
              image {
                childImageSharp {
                  fluid(quality: 100) {
                    ...GatsbyImageSharpFluid
                    src
                  }
                }
              }
            }
          }
        }
      `}
      render={data => <Featured data={data} {...props} />}
    />
  )
}

Featured.propTypes = {
  data: PropTypes.shape({
    mdx: PropTypes.shape({
      fields: PropTypes.shape({
        slug: PropTypes.any.isRequired,
      }).isRequired,
      body: PropTypes.any.isRequired,
      frontmatter: PropTypes.shape({
        categories: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
}

FeaturedBlogButton.propTypes = {
  path: PropTypes.string.isRequired,
}

const FeaturedWrapper = styled(motion.div)`
  padding: 80px 10%;
  ${media.thone`padding: 100px 0px;`}
`
const FeaturedEntry = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-auto-rows: 600px;
  ${media.tablet`grid-template-columns: 1fr ; grid-template-rows: repeat(2,1fr);`}
`
const FeaturedImage = styled(motion.div)`
  background-size: cover;
  background-position: center;
  width: 100%;
`
const FeaturedContent = styled.div`
  display: grid;
  grid-template-columns: repeat(6, 1fr);
  padding: 60px;
  h1 {
    grid-column-start: span 6;
    font-size: 72px;
    text-transform: uppercase;
    ${media.bigDesktop`font-size: 38px;`}
    ${media.tablet`padding: 29px 0;`}
  }
  background: ${props => props.theme.secondary};
`
const FeaturedButton = styled(motion.button)`
  width: 100%;
  height: 39px;
  background: ${props => props.theme.tietary};
  color: ${props => props.theme.primary};
  p {
    font-size: 14px;
    color: ${props => props.theme.primary};
  }
  transition: all 0.4s ease;
  &:hover {
    transition: all 0.4s ease;
    background: none;
    p {
      color: ${props => props.theme.tietary};
    }

    border: 1px solid ${props => props.theme.tietary};
  }
`
