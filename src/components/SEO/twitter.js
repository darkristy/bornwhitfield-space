import React from 'react'
import Helmet from 'react-helmet'
import PropTypes from 'prop-types'

const Twitter = ({
  twitterCard,
  twitterSite,
  twitterTitle,
  twitterDescription,
  twitterImage,
}) => {
  return (
    <Helmet>
      <meta name='twitter:card' content={twitterCard} />
      <meta name='twitter:site' content={twitterSite} />
      <meta name='twitter:title' content={twitterTitle} />
      <meta name='twitter:description' content={twitterDescription} />
      <meta name='twitter:image' content={twitterImage} />
    </Helmet>
  )
}

export default Twitter

Twitter.propTypes = {
  twitterCard: PropTypes.string.isRequired,
  twitterSite: PropTypes.string.isRequired,
  twitterTitle: PropTypes.string.isRequired,
  twitterDescription: PropTypes.string.isRequired,
  twitterImage: PropTypes.string.isRequired,
}
