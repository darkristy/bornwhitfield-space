import React from 'react'
import Helmet from 'react-helmet'
import PropTypes from 'prop-types'

const Facebook = ({
  facebookUrl,
  facebookType,
  facebookTitle,
  facebookDescription,
  facebookImage,
}) => {
  return (
    <Helmet>
      <meta property='og:url' content={facebookUrl} />
      <meta property='og:type' content={facebookType} />
      <meta property='og:title' content={facebookTitle} />
      <meta property='og:description' content={facebookDescription} />
      <meta property='og:image' content={facebookImage} />
    </Helmet>
  )
}

export default Facebook

Facebook.propTypes = {
  facebookUrl: PropTypes.string.isRequired,
  facebookType: PropTypes.string.isRequired,
  facebookTitle: PropTypes.string.isRequired,
  facebookDescription: PropTypes.string.isRequired,
  facebookImage: PropTypes.string.isRequired,
}
