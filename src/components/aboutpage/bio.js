import React from 'react'
import styled from 'styled-components'
import { graphql, useStaticQuery } from 'gatsby'
import Img from 'gatsby-image'
import media from '../../utils/media'

const Bio = () => {
  const data = useStaticQuery(graphql`
    query BioImg {
      bioImg: file(relativePath: { eq: "misc/bio-edit.jpg" }) {
        childImageSharp {
          fluid(maxWidth: 1000, quality: 100) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  const image = data.bioImg.childImageSharp.fluid
  return (
    <About>
      <BioContainer>
        <BioImage>
          <Img fluid={image} />
        </BioImage>
        <BioCopy>
          <SubTitle>
            <h2>About Me</h2>
          </SubTitle>
          <Copy>
            <p>
              I’m a young graphic designer currently based in Wilmington NC. I’m
              currently working on a contract basis with a creative/design
              agency, but open to freelance.
            </p>
            <br />
            <p>
              The focus right now for me is to build upon my skillset with
              design and frontend development at the for front.
            </p>
          </Copy>
        </BioCopy>
      </BioContainer>
    </About>
  )
}

export default Bio

const About = styled.div`
  padding: 100px 10%;
`
const BioContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  grid-auto-rows: auto;
  ${media.desktop`grid-template-columns: none;grid-template-rows: repeat(2, auto);`}
`
const BioImage = styled.div`
  grid-column: span 6;
  margin-right: 20%;
  ${media.desktop`margin-right: 0;`}
`
const BioCopy = styled.div`
  grid-column: span 6;
  display: flex;
  padding-top: 200px;
  ${media.bigDesktop`padding-top: 80px;`}
`
const SubTitle = styled.div`
  h2 {
    font-size: 24px;
    text-transform: uppercase;
    ${media.tablet`font-size: 18px;`}
  }
  margin-right: 100px;
  ${media.thone`margin-right: 35px;`}
`
const Copy = styled.div`
  max-width: 290px;
  ${media.thone`max-width: 220px;`}
`
export const Experties = styled.h2`
  font-size: 1.45rem;
  margin: 0 20%;
  padding-bottom: 60px;
  text-transform: uppercase;
  span {
    margin: 0 0.625rem;
    color: ${props => props.theme.tietary};
  }

  ${media.tablet`font-size: 1rem;`}
  ${media.desktop`margin: 0;`}
`
export const Tools = styled.h2`
  font-size: 1.45rem;
  text-transform: uppercase;
  margin: 0 20%;

  span {
    margin: 0 0.625rem;
    color: ${props => props.theme.tietary};
  }
  ${media.tablet`font-size: 1rem;`}
  ${media.desktop`margin: 0`}
`
