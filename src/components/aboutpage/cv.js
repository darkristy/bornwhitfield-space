import React from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import { experiences, experties, tools } from '../../constants/cv-data'
import { Experties, Tools } from './bio'

const CV = () => {
  return (
    <>
      <Main>
        <LeftColumn>
          <HeadingCopy>
            <h2>education</h2>
          </HeadingCopy>
          <EntryItem>
            <Year>AUG 2015 — DEC 2019</Year>
            <p>B.F.A. Graphic Design - Liberty Univeristy</p>
          </EntryItem>
          <br />
          <br />
          <HeadingCopy>
            <h2>experience</h2>
          </HeadingCopy>
          {experiences.map(({ key, year, position }) => (
            <EntryItem key={key}>
              <Year>{year}</Year>
              <p>{position}</p>
            </EntryItem>
          ))}
        </LeftColumn>
        <RightColumn>
          <Experties>
            <HeadingCopy style={{ paddingBottom: 20 }}>
              <h2>What I Do</h2>
            </HeadingCopy>
            <div style={{ color: 'gray' }}>
              {experties.map((s, idx) => (
                <React.Fragment key={s.name}>
                  {s.name}
                  {idx === experties.length - 1 ? '' : <span> / </span>}
                </React.Fragment>
              ))}
            </div>
          </Experties>
          <Tools>
            <HeadingCopy style={{ paddingBottom: 20 }}>
              <h2>Tools I Use</h2>
            </HeadingCopy>
            <div style={{ color: 'gray' }}>
              {tools.map((s, idx) => (
                <React.Fragment key={s.name}>
                  {s.name}
                  {idx === tools.length - 1 ? '' : <span> / </span>}
                </React.Fragment>
              ))}
            </div>
          </Tools>
        </RightColumn>
      </Main>
      <ResumeButton>
        <h4>View Resume</h4>
      </ResumeButton>
    </>
  )
}

export default CV

const Main = styled.div`
  padding: 10vw 10% 20px;
  display: grid;
  grid-template-columns: 1fr 2fr;
  ${media.desktop`grid-template-columns: none;grid-template-rows: repeat(2, 1fr); `}
`
const LeftColumn = styled.div``
const RightColumn = styled.div`
  ${media.desktop`padding-top:100px;`}
`
const HeadingCopy = styled.div`
  h2 {
    text-transform: uppercase;
    font-size: 48px;
  }
`
const Year = styled.h2`
  font-size: 18px;
`
const EntryItem = styled.div`
  padding: 20px 0;
`
const ResumeButton = styled.div`
  padding: 0 10%;
`
