import React, { useEffect, useRef, useContext, useState } from 'react'
import styled from 'styled-components'

import gsap, { Power3 } from 'gsap'
import { useInView } from 'react-intersection-observer'
import { motion, useAnimation } from 'framer-motion'
import media from '../../utils/media'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import Context from '../../context/context'
import Img from 'gatsby-image'
import { graphql, useStaticQuery } from 'gatsby'

const Dailogue = () => {
  // let dailogueRef = useRef(null)
  let firstChild = useRef(null)
  let secondChild = useRef(null)
  let thirdChild = useRef(null)
  let fourthChild = useRef(null)
  /*eslint-disable */
  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
    rootMargin: '-200px',
  })
  const animation = useAnimation()

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])

  const data = useStaticQuery(graphql`
    query AboutImg {
      hero2: file(relativePath: { eq: "misc/Polar1.png" }) {
        childImageSharp {
          fluid(maxWidth: 1000, quality: 100) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)

  const image = data.hero2.childImageSharp.fluid

  const line1 = {
    visible: {
      // opacity: 1,
      y: 0,
      transition: {
        duration: 0.6,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      // opacity: 0,
      y: 83,
    },
  }
  const line2 = {
    visible: {
      // opacity: 1,
      y: 0,
      transition: {
        delay: 0.2,
        duration: 0.5,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      // opacity: 0,
      y: 83,
    },
  }
  const line3 = {
    visible: {
      // opacity: 1,
      y: 0,
      transition: {
        delay: 0.4,
        duration: 0.5,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      // opacity: 0,
      y: 83,
    },
  }
  const line4 = {
    visible: {
      // opacity: 1,
      y: 0,
      transition: {
        delay: 0.6,
        duration: 0.5,
        ease: [0.6, 0.05, -0.01, 0.9],
      },
    },
    hidden: {
      // opacity: 0,
      y: 83,
    },
  }

  const [hovered, setHovered] = useState(false)

  // const [t1] = useState(new gsap.timeline({ delay: 0.2 }))
  // useEffect(() => {
  //   gsap.set('.wrapper', {
  //     css: { visibility: 'hidden' },
  //   })
  //   if (inView === true) {
  //     gsap.set('.wrapper', {
  //       css: { visibility: 'visible' },
  //     })
  //     t1.from([firstChild, secondChild, thirdChild, fourthChild], 0.6, {
  //       y: 83,
  //       ease: Power3.easeOut,
  //       stagger: {
  //         amount: 0.5,
  //       },
  //       delay: 0.6,
  //     })
  //   }
  // }, [t1, inView, firstChild, secondChild, thirdChild, fourthChild])

  return (
    <TextContainer>
      <motion.div ref={ref} className='wrapper'>
        <div>
          <h1>
            <Text>
              <motion.div animate={animation} initial='hidden' variants={line1}>
                My name is <span className='red'>Kahlil </span>
              </motion.div>
            </Text>
            <Text>
              <motion.div animate={animation} initial='hidden' variants={line2}>
                <span className='red'>whifield</span>, a designer
              </motion.div>
            </Text>
            <Text>
              <motion.div animate={animation} initial='hidden' variants={line3}>
                Based in wilmington,
              </motion.div>
            </Text>
            <Text>
              <motion.div animate={animation} initial='hidden' variants={line4}>
                North Carolina.
              </motion.div>
            </Text>
          </h1>
          <div className='textbox'>
            <p>I am also a photographer on the side.</p>
          </div>
        </div>

        <motion.div
          className='about-image'
          animate={animation}
          initial='hidden'
          variants={{
            visible: {
              opacity: 1,
              y: 0,
              transition: {
                delay: 0.2,
                duration: 0.5,
                ease: [0.6, 0.05, -0.01, 0.9],
              },
            },
            hidden: {
              opacity: 0,
              y: 83,
            },
          }}
        >
          <DailogueImage>
            <Img className='hero' fluid={image} style={{ maxWidth: '100%' }} />
          </DailogueImage>

          <AniLink cover direction='down' bg='black' to='/about'>
            <motion.div
              onHoverStart={() => setHovered(!hovered)}
              onHoverEnd={() => setHovered(!hovered)}
              onMouseEnter={() => onCursor('pointer')}
              onMouseLeave={onCursor}
              className='linked'
            >
              <h1>More About Me</h1>
              <span className='arrow'>
                <motion.svg
                  animate={{ x: hovered ? 48 : 0 }}
                  transition={{ duration: 0.6, ease: [0.6, 0.05, -0.01, 0.9] }}
                  xmlns='http://www.w3.org/2000/svg'
                  viewBox='0 0 101 57'
                >
                  <path
                    d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                    fill='#FFF'
                    fillRule='evenodd'
                  ></path>
                </motion.svg>
              </span>
            </motion.div>
          </AniLink>
        </motion.div>
      </motion.div>
    </TextContainer>
  )
}

export default Dailogue

const Text = styled.div`
  overflow: hidden;
  height: 83;
`
const TextContainer = styled.div`
  padding: 100px 10%;
  ${media.desktop`padding:100px 10%;`};
  margin: 20% 0;
  display: flex;
  justify-content: center;
  .wrapper {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-template-rows: auto;
    grid-column-gap: 220px;
    ${media.giant`
    grid-template-columns: 1fr;
    `};
  }

  ${media.tablet` margin: 60% 0;`};
  .red {
    color: #f64150;
  }
  h1 {
    width: 100%;
    max-width: 700px;
    font-size: 72px;
    text-transform: uppercase;
    transition: ease 0.4s;
    ${media.bigDesktop`font-size: 64px;`};
    ${media.tablet`font-size: 48px;`};
    ${media.thone`font-size: 44px;`};
    ${media.phone`font-size: 32px;`};
    .text {
      visibility: hidden;
    }
  }
  p {
    padding-top: 36px;
    width: 158px;
  }
  .textbox {
    width: 158px;
    overflow: hidden;
  }

  .about-image {
    position: relative;
    .linked {
      position: absolute;
      bottom: -128px;
      h1 {
        font-size: 5.6rem;
        width: 420px;
        ${media.phone`font-size: 2.2rem; width: 140px;`};
      }
      .arrow {
        width: 124px;
        height: 80px;
        display: block;
        position: relative;
        overflow: hidden;
        svg {
          position: absolute;
          top: 16px;
          left: -48px;
          width: 108px;
          path {
            fill: ${props => props.theme.tietary};
          }
        }
      }
    }
  }
`

const DailogueImage = styled.div`
  width: 60%;
  height: 90%;
  margin-top: -100px;
  ${media.giant`margin-top: 0;`}
`
