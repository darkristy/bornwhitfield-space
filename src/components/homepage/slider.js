import React, { useRef, useEffect, useState, useContext } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import media from '../../utils/media'
import { SliderButton, Seperator } from './hero'
import gsap from 'gsap'
import BackgroundImage from 'gatsby-background-image'
import AniLink from 'gatsby-plugin-transition-link/AniLink'

import useGetWindow from '../../hooks/getWindow'

import {
  slideLeft,
  scale,
  slideRight,
  infoFadeIn,
  infoFadeOut,
  numFadeIn,
  numFadeOut,
} from '../../animations/slider-anim'

import Context from '../../context/context'

import PropTypes from 'prop-types'

const sliderNum = [{ num: 1 }, { num: 2 }, { num: 3 }]

const pathPrefix = `/work/`

const Slider = ({ data }) => {
  let imageList = useRef(null)
  let infoList = useRef(null)
  let numList = useRef(null)

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  const [active, setActive] = useState({
    isActive1: true,
    isActive2: false,
    isActive3: false,
  })

  const [viewport, setViewport] = useState({
    width: null,
  })

  const { dimensions } = useGetWindow()

  useEffect(() => {
    if (dimensions.width > 1800) {
      setViewport({ width: 980 })
    } else {
      setViewport({ width: 1410 })
    }
  }, [dimensions.width])

  const imageWidth = viewport.width

  const hoverIn = () => {
    gsap.set('.img', {
      css: { filter: 'grayscale(0)' },
    })
  }

  const hoverOut = () => {
    gsap.set('.img', {
      css: { filter: 'grayscale(100%)' },
    })
  }

  const nextSlide = () => {
    if (imageList.children[0].classList.contains('active')) {
      setActive({ isActive1: false, isActive2: true })
      slideLeft(imageList.children, imageWidth, 0, 1)
      slideLeft(imageList.children, imageWidth, 1, 1)
      scale(imageList.children, 1, 1)
      slideLeft(imageList.children, imageWidth, 2, 1)
      slideLeft(imageList.children, imageWidth, 2, 0)
      numFadeOut(numList.children, 0, 0.7)
      numFadeIn(numList.children, 1, 0.7)
      infoFadeOut(infoList.children, 0, 0.7)
      infoFadeIn(infoList.children, 1, 0.7)
    } else if (imageList.children[1].classList.contains('active')) {
      setActive({ isActive2: false, isActive3: true })
      slideRight(imageList.children, imageWidth, 0, 1)
      slideLeft(imageList.children, imageWidth, 1, 1, 2)
      slideLeft(imageList.children, imageWidth, 2, 1, 2)
      scale(imageList.children, 2, 1)
      numFadeOut(numList.children, 1, 0.7)
      numFadeIn(numList.children, 2, 0.7)
      infoFadeOut(infoList.children, 1, 0.7)
      infoFadeIn(infoList.children, 2, 0.7)
    } else if (imageList.children[2].classList.contains('active')) {
      setActive({ isActive1: true, isActive3: false })
      slideLeft(imageList.children, imageWidth, 2, 1, 3)
      slideLeft(imageList.children, imageWidth, 0, 1, 0)
      slideLeft(imageList.children, imageWidth, 1, 0, 0)
      scale(imageList.children, 0, 1)
      numFadeOut(numList.children, 2, 0.7)
      numFadeIn(numList.children, 0, 0.7)
      infoFadeOut(infoList.children, 2, 0.7)
      infoFadeIn(infoList.children, 0, 0.7)
    }
  }
  const prevSlide = () => {
    if (imageList.children[0].classList.contains('active')) {
      setActive({ isActive1: false, isActive3: true })
      slideLeft(imageList.children, imageWidth, 2, 0, 3)
      slideLeft(imageList.children, imageWidth, 2, 1, 2)
      scale(imageList.children, 2, 1)
      slideRight(imageList.children, imageWidth, 0, 1)
      slideRight(imageList.children, imageWidth, 1, 1)
      numFadeOut(numList.children, 0, 0.7)
      numFadeIn(numList.children, 2, 1)
      infoFadeOut(infoList.children, 0, 0.7)
      infoFadeIn(infoList.children, 2, 1)
    } else if (imageList.children[1].classList.contains('active')) {
      setActive({ isActive2: false, isActive1: true })
      slideLeft(imageList.children, imageWidth, 0, 0)
      slideRight(imageList.children, imageWidth, 0, 1, 0)
      slideRight(imageList.children, imageWidth, 1, 1, 0)
      slideRight(imageList.children, imageWidth, 2, 1, 2)
      scale(imageList.children, 0, 1)
      numFadeOut(numList.children, 1, 0.7)
      numFadeIn(numList.children, 0, 1)
      infoFadeOut(infoList.children, 1, 0.7)
      infoFadeIn(infoList.children, 0, 1)
    } else if (imageList.children[2].classList.contains('active')) {
      setActive({ isActive2: true, isActive3: false })
      slideLeft(imageList.children, imageWidth, 2, 1)
      slideLeft(imageList.children, imageWidth, 1, 0, 2)
      slideLeft(imageList.children, imageWidth, 1, 1)
      scale(imageList.children, 1, 1)
      numFadeOut(numList.children, 2, 0.7)
      numFadeIn(numList.children, 1, 1)
      infoFadeOut(infoList.children, 2, 0.7)
      infoFadeIn(infoList.children, 1, 1)
    }
  }
  return (
    <SliderContainer>
      <SliderStack>
        <SliderWrapper>
          <SliderContent>
            <ul ref={el => (imageList = el)}>
              {data.allMdx.edges.slice(0, 1).map(edge => {
                const backgroundImageStack = [
                  edge.node.frontmatter.image.childImageSharp.fluid,
                  `linear-gradient(
                    to bottom,
                    rgba(10,10,10,0) 0%,
                    rgba(10,10,10,0) 15%,
                    rgba(10,10,10,0.9) 100%)`,
                ].reverse()
                return (
                  <SliderImage
                    key={edge.node.id}
                    className={active.isActive1 ? 'active' : ''}
                  >
                    <BackgroundImage
                      className='img'
                      fluid={backgroundImageStack}
                    />
                  </SliderImage>
                )
              })}
              {data.allMdx.edges.slice(1, 2).map(edge => {
                const backgroundImageStack = [
                  edge.node.frontmatter.image.childImageSharp.fluid,
                  `linear-gradient(
                    to bottom,
                    rgba(10,10,10,0) 0%,
                    rgba(10,10,10,0) 15%,
                    rgba(10,10,10,0.9) 100%)`,
                ].reverse()
                return (
                  <SliderImage
                    key={edge.node.id}
                    className={active.isActive2 ? 'active' : ''}
                  >
                    <BackgroundImage
                      className='img'
                      fluid={backgroundImageStack}
                    />
                  </SliderImage>
                )
              })}
              {data.allMdx.edges.slice(2).map(edge => {
                const backgroundImageStack = [
                  edge.node.frontmatter.image.childImageSharp.fluid,
                  `linear-gradient(
                    to bottom,
                    rgba(10,10,10,0) 0%,
                    rgba(10,10,10,0) 15%,
                    rgba(10,10,10,0.9) 100%)`,
                ].reverse()
                return (
                  <SliderImage
                    key={edge.node.id}
                    className={active.isActive3 ? 'active' : ''}
                  >
                    <BackgroundImage
                      className='img'
                      fluid={backgroundImageStack}
                    />
                  </SliderImage>
                )
              })}
            </ul>
            <SliderNavigation>
              <SliderIndicator>
                <ul ref={el => (numList = el)}>
                  <li className={active.isActive1 ? 'active' : ''}>
                    <h4>{sliderNum[0].num}</h4>
                  </li>
                  <li className={active.isActive2 ? 'active' : ''}>
                    <h4>{sliderNum[1].num}</h4>
                  </li>
                  <li className={active.isActive3 ? 'active' : ''}>
                    <h4>{sliderNum[2].num}</h4>
                  </li>
                </ul>
                <Seperator>
                  <div className='seperator' />
                </Seperator>
                <h4>{sliderNum[2].num}</h4>
              </SliderIndicator>
              <SliderArrows>
                <h4
                  onClick={prevSlide}
                  onMouseEnter={() => onCursor('hovered')}
                  onMouseLeave={onCursor}
                >
                  PREV
                </h4>
                <h4
                  onClick={nextSlide}
                  onMouseEnter={() => onCursor('hovered')}
                  onMouseLeave={onCursor}
                >
                  NEXT
                </h4>
              </SliderArrows>
            </SliderNavigation>
          </SliderContent>
        </SliderWrapper>

        <SliderInfo>
          <ul ref={el => (infoList = el)}>
            {data.allMdx.edges.map(edge => (
              <li key={edge.node.id}>
                <SliderTitle>
                  <h1>{edge.node.frontmatter.title}</h1>
                  <h4 className='desc'>{edge.node.frontmatter.desc}</h4>
                </SliderTitle>
                <SliderButton
                  onMouseEnter={() => {
                    hoverIn()
                    onCursor('w-button')
                  }}
                  onMouseLeave={() => {
                    hoverOut()
                    onCursor()
                  }}
                >
                  <AniLink
                    cover
                    direction='down'
                    bg='black'
                    to={`${pathPrefix}${edge.node.fields.slug}`}
                  >
                    <h4>View more</h4>
                  </AniLink>
                </SliderButton>
              </li>
            ))}
          </ul>
        </SliderInfo>
      </SliderStack>
    </SliderContainer>
  )
}
export default props => (
  <StaticQuery
    query={graphql`
      query silderList {
        allMdx(
          limit: 3
          filter: {
            frontmatter: { type: { eq: "work" }, published: { eq: true } }
          }
          sort: { fields: [frontmatter___date], order: DESC }
        ) {
          edges {
            node {
              id
              fields {
                slug
              }
              frontmatter {
                title
                desc
                image {
                  childImageSharp {
                    fluid(maxWidth: 1200, quality: 100) {
                      ...GatsbyImageSharpFluid
                      src
                    }
                  }
                }
              }
            }
          }
        }
      }
    `}
    render={data => <Slider data={data} {...props} />}
  />
)

Slider.propTypes = {
  data: PropTypes.shape({
    allMdx: PropTypes.shape({
      edges: PropTypes.array.isRequired,
    }).isRequired,
  }).isRequired,
}

const SliderContainer = styled.div`
  padding: 0 10%;
  @media screen and (max-width: 1200px) {
    padding: 0;
  }
`
const SliderStack = styled.div`
  display: flex;
  @media screen and (max-width: 1800px) {
    flex-direction: column;
  }
`

// const SliderCategory = styled.h4``

const SliderWrapper = styled.div`
  ul {
    display: flex;
    width: 100%;
    max-width: 980px;
    overflow: hidden;
    position: relative;
    @media screen and (max-width: 1800px) {
      max-width: 1410px;
    }
  }
`
const SliderContent = styled.div`
  padding-top: 190px;
`
const SliderImage = styled.li`
  .img {
    width: 980px;
    height: 680px;
    background-size: cover;
    background-position: center;
    transition: ease 0.4s;
    filter: grayscale(100%);
    @media screen and (max-width: 1800px) {
      width: 1410px;
    }
  }
`
const SliderTitle = styled.div`
  padding-top: 290px;

  h1 {
    color: ${props => props.theme.tietary};
    font-size: 68px;
    transition: ease 0.4s;
    text-transform: uppercase;
    @media screen and (min-width: 800px) {
      width: 500px;
    }
    ${media.thone`font-size: 52px; `}
  }
  ${media.desktop`padding-top: 190px; `}
`
const SliderNavigation = styled.div`
  display: flex;
  flex-shrink: 2;
  max-width: 980px;
  justify-content: space-between;
  padding-top: 21px;
  ${media.desktop`padding: 21px 4%; `}
  @media screen and (max-width: 1800px) {
    max-width: 1410px;
  }
`

const SliderArrows = styled.div`
  display: flex;
  h4 {
    font-size: 16px;
    transition: ease 0.4s;
    &:nth-of-type(1) {
      padding-right: 31px;
    }
    &:hover {
      color: #f64150;
    }
  }
`
const SliderIndicator = styled.div`
  display: flex;
  h4 {
    font-size: 16px;
  }
  ul {
    position: relative;
    overflow: hidden;
    width: 10px;
    li {
      width: 10px;

      position: absolute;
      opacity: 0;
      &:nth-of-type(1) {
        opacity: 1;
      }
    }
  }
`
const SliderInfo = styled.div`
  padding: 0 5%;
  p {
    color: white;
  }
  ul {
    overflow: hidden;
    position: absolute;
    height: 706px;
    width: 100%;
    max-width: 580px;
    @media screen and (max-width: 1800px) {
      margin-top: -740px;
    }
    li {
      h4 {
      }
      .desc {
        width: 480px;

        padding-top: 10px;
      }

      /* display: flex; */
      position: absolute;
      visibility: hidden;
      opacity: 0;
      ${media.thone` .desc {
        width: 380px;}`}
      :nth-of-type(1) {
        opacity: 1;
        visibility: visible;
      }
    }
  }
`
