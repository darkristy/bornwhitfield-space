import React, { useContext } from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import { motion } from 'framer-motion'
import Context from '../../context/context'

const HeroCTA = () => {
  const link = `mailto:bornwhitfield@gmail.com`

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  return (
    <CTAContainer>
      <div>
        <HeroCTAHeading>
          <h1>Get in touch</h1>
          <h1>Get in touch</h1>
        </HeroCTAHeading>
        <CTAInfoBox>
          <div>
            <CTACopy>
              <p>
                If you like what I am putting out I encourage you to view more,
                but if you have something in mind. Dont be shy, say hello.
              </p>
            </CTACopy>

            <CTAButton
              onMouseEnter={() => onCursor('w-button')}
              onMouseLeave={onCursor}
              animate
            >
              <a href={link}>
                <p>say hello</p>
              </a>
            </CTAButton>
          </div>
        </CTAInfoBox>
      </div>
    </CTAContainer>
  )
}

export default HeroCTA

const CTAContainer = styled(motion.div)`
  padding: 320px 10% 40px;

  ${media.desktop`padding:120px 10%;`}
  display: flex;
  justify-content: center;
`

const CTAInfoBox = styled.div`
  display: flex;
  justify-content: center;
`
const CTACopy = styled.div`
  p {
    width: 295px;
    font-size: 14px;
    padding-bottom: 56px;
  }
  margin-right: 26px;
`

const HeroCTAHeading = styled.div`
  padding-bottom: 48px;
  ${media.thone`padding-bottom: 24px;`}
  h1 {
    font-size: 100px;
    ${media.desktop`font-size: 84px; max-width: 611px;`}
    ${media.thone`font-size: 56px;max-width: 411px; padding-bottom: 36px;`};
    &:nth-of-type(2) {
      margin-top: -98px;
      ${media.desktop` margin-top: -86px; `}
      ${media.tablet` margin-top: -78px; `}
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 0.6px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
    text-transform: uppercase;
    width: 100%;
    max-width: 811px;
  }
`

const CTAButton = styled(motion.div)`
  background: none;
  border: 1px solid ${props => props.theme.tietary};
  margin-bottom: 200px;
  p {
    color: ${props => props.theme.tietary};
    font-size: 14px;
    text-transform: uppercase;
  }
  height: 50px;
  padding: 17px 20px;
  width: 100%;
  max-width: 310px;
  text-align: center;
  transition: all 0.4s ease;
  &:hover {
    background: ${props => props.theme.tietary};
    p {
      color: ${props => props.theme.primary};
    }
    border: none;
  }
`
