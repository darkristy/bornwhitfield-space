// import React, { useRef, useEffect, useState } from 'react'
// import { StaticQuery, graphql } from 'gatsby'
// import styled from 'styled-components'
// import media from '../../utils/media'
// import { SliderButton, Seperator } from './hero'
// import gsap, { Power3 } from 'gsap'
// import BackgroundImage from 'gatsby-background-image'
// import AniLink from 'gatsby-plugin-transition-link/AniLink'
// // import {
// //   useGlobalStateContext,
// //   useGlobalDispatchContext,
// // } from '../../context/globalContext'
// import PropTypes from 'prop-types'

// const sliderNum = [{ num: 1 }, { num: 2 }, { num: 3 }]

// const pathPrefix = `/work/`

// const Slider = ({ data }) => {
//   let imageList = useRef(null)
//   let numList = useRef(null)
//   // const { cursorStyles } = useGlobalStateContext()
//   // const dispatch = useGlobalDispatchContext()
//   // const onCursor = cursorType => {
//   //   cursorType = (cursorStyles.includes(cursorType) && cursorType) || false
//   //   dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
//   // }
//   const [state, setState] = useState({
//     isActive1: true,
//     isActive2: false,
//     isActive3: false,
//   })
//   const imageWidth = 1280
//   useEffect(() => {
//     gsap.to(imageList.children[0], 0, {})
//   })
//   const slideLeft = (index, duration, multipled = 1) => {
//     gsap.to(imageList.children[index], duration, {
//       x: -imageWidth * multipled,
//       ease: Power3.easeOut,
//     })
//   }
//   const slideRight = (index, duration, multipled = 1) => {
//     gsap.to(imageList.children[index], duration, {
//       x: imageWidth * multipled,
//       ease: Power3.easeOut,
//     })
//   }
//   const scale = (index, duration) => {
//     gsap.from(imageList.children[index], duration, {
//       scale: 1.2,
//       ease: Power3.easeOut,
//     })
//   }
//   const fadeOut = (index, duration) => {
//     gsap.to(numList.children[index], duration, {
//       opacity: 0,
//     })
//   }
//   const fadeIn = (index, duration) => {
//     gsap.to(numList.children[index], duration, {
//       opacity: 1,
//       delay: 0.7,
//     })
//   }
//   const nextSlide = () => {
//     if (imageList.children[0].classList.contains('active')) {
//       setState({ isActive1: false, isActive2: true })
//       slideLeft(0, 1)
//       slideLeft(1, 1)
//       scale(1, 1)
//       slideLeft(2, 1)
//       slideLeft(2, 0)
//       fadeOut(0, 0.8)
//       fadeIn(1, 0.8)
//     } else if (imageList.children[1].classList.contains('active')) {
//       setState({ isActive2: false, isActive3: true })
//       slideRight(0, 1)
//       slideLeft(1, 1, 2)
//       slideLeft(2, 1, 2)
//       scale(2, 1)
//       fadeOut(1, 0.7)
//       fadeIn(2, 0.7)
//     } else if (imageList.children[2].classList.contains('active')) {
//       setState({ isActive1: true, isActive3: false })
//       slideLeft(2, 1, 3)
//       slideLeft(0, 1, 0)
//       slideLeft(1, 0, 0)
//       scale(0, 1)
//       fadeOut(2, 0.7)
//       fadeIn(0, 0.7)
//     }
//   }
//   const prevSlide = () => {
//     if (imageList.children[0].classList.contains('active')) {
//       setState({ isActive1: false, isActive3: true })
//       slideLeft(2, 0, 3)
//       slideLeft(2, 1, 2)
//       scale(2, 1)
//       slideRight(0, 1)
//       slideRight(1, 1)
//       fadeOut(0, 0.7)
//       fadeIn(2, 0.7)
//     } else if (imageList.children[1].classList.contains('active')) {
//       setState({ isActive2: false, isActive1: true })
//       slideLeft(0, 0)
//       slideRight(0, 1, 0)
//       slideRight(1, 1, 0)
//       slideRight(2, 1, 2)
//       scale(0, 1)
//       fadeOut(1, 0.7)
//       fadeIn(0, 0.7)
//     } else if (imageList.children[2].classList.contains('active')) {
//       setState({ isActive2: true, isActive3: false })
//       slideLeft(2, 1)
//       slideLeft(1, 0, 2)
//       slideLeft(1, 1)
//       scale(1, 1)
//       fadeOut(2, 0.7)
//       fadeIn(1, 0.7)
//     }
//   }
//   return (
//     <SliderContainer>
//       <SliderWrapper>
//         <SliderContent>
//           <ul ref={el => (imageList = el)}>
//             {data.allMdx.edges.slice(0, 1).map(edge => {
//               const backgroundImageStack = [
//                 edge.node.frontmatter.image.childImageSharp.fluid,
//                 `linear-gradient(
//                       to bottom,
//                       rgba(10,10,10,0) 0%,
//                       rgba(10,10,10,0) 5%,
//                       rgba(10,10,10,0.4) 100%)`,
//               ].reverse()
//               return (
//                 <SliderImage
//                   key={edge.node.id}
//                   className={state.isActive1 ? 'active' : ''}
//                 >
//                   <AniLink
//                     cover
//                     direction='down'
//                     bg='black'
//                     to={`${pathPrefix}${edge.node.fields.slug}`}
//                   >
//                     <BackgroundImage
//                       className='img'
//                       fluid={backgroundImageStack}
//                     >
//                       <SliderTitle>
//                         <SliderCategory>concept</SliderCategory>
//                         <h1>{edge.node.frontmatter.title}</h1>
//                       </SliderTitle>
//                     </BackgroundImage>
//                   </AniLink>
//                 </SliderImage>
//               )
//             })}
//             {data.allMdx.edges.slice(1, 2).map(edge => {
//               const backgroundImageStack = [
//                 edge.node.frontmatter.image.childImageSharp.fluid,
//                 `linear-gradient(
//                       to bottom,
//                       rgba(10,10,10,0) 0%,
//                       rgba(10,10,10,0) 5%,
//                       rgba(10,10,10,0.4) 100%)`,
//               ].reverse()
//               return (
//                 <SliderImage
//                   key={edge.node.id}
//                   className={state.isActive2 ? 'active' : ''}
//                 >
//                   <AniLink
//                     cover
//                     direction='down'
//                     bg='black'
//                     to={`${pathPrefix}${edge.node.fields.slug}`}
//                   >
//                     <BackgroundImage
//                       className='img'
//                       fluid={backgroundImageStack}
//                     >
//                       <SliderTitle>
//                         <SliderCategory>concept</SliderCategory>
//                         <h1>{edge.node.frontmatter.title}</h1>
//                       </SliderTitle>
//                     </BackgroundImage>
//                   </AniLink>
//                 </SliderImage>
//               )
//             })}
//             {data.allMdx.edges.slice(2).map(edge => {
//               const backgroundImageStack = [
//                 edge.node.frontmatter.image.childImageSharp.fluid,
//                 `linear-gradient(
//                       to bottom,
//                       rgba(10,10,10,0) 0%,
//                       rgba(10,10,10,0) 5%,
//                       rgba(10,10,10,0.4) 100%)`,
//               ].reverse()
//               return (
//                 <SliderImage
//                   key={edge.node.id}
//                   className={state.isActive3 ? 'active' : ''}
//                 >
//                   <AniLink
//                     cover
//                     direction='down'
//                     bg='black'
//                     to={`${pathPrefix}${edge.node.fields.slug}`}
//                   >
//                     <BackgroundImage
//                       className='img'
//                       fluid={backgroundImageStack}
//                     >
//                       <SliderTitle>
//                         <SliderCategory>concept</SliderCategory>
//                         <h1>{edge.node.frontmatter.title}</h1>
//                       </SliderTitle>
//                     </BackgroundImage>
//                   </AniLink>
//                 </SliderImage>
//               )
//             })}
//           </ul>
//         </SliderContent>
//         <div
//           style={{
//             display: 'flex',
//             justifyContent: 'center',
//           }}
//         >
//           <SliderNavigation>
//             <SliderIndicator>
//               <ul ref={el => (numList = el)}>
//                 <li className={state.isActive1 ? 'active' : ''}>
//                   <h4>{sliderNum[0].num}</h4>
//                 </li>
//                 <li className={state.isActive2 ? 'active' : ''}>
//                   <h4>{sliderNum[1].num}</h4>
//                 </li>
//                 <li className={state.isActive3 ? 'active' : ''}>
//                   <h4>{sliderNum[2].num}</h4>
//                 </li>
//               </ul>
//               <Seperator>
//                 <div className='seperator' />
//               </Seperator>
//               <h4>{sliderNum[2].num}</h4>
//             </SliderIndicator>
//             <SliderArrows>
//               <h4
//                 onClick={prevSlide}
//                 // onMouseEnter={() => onCursor('hovered')}
//                 // onMouseLeave={onCursor}
//               >
//                 PREV
//               </h4>
//               <h4
//                 onClick={nextSlide}
//                 // onMouseEnter={() => onCursor('hovered')}
//                 // onMouseLeave={onCursor}
//               >
//                 NEXT
//               </h4>
//             </SliderArrows>
//           </SliderNavigation>
//         </div>
//         <div style={{ display: 'flex', justifyContent: 'center' }}>
//           <SliderButton>
//             <AniLink cover direction='down' bg='black' to={`/works`}>
//               <h1>View more work</h1>
//             </AniLink>
//           </SliderButton>
//         </div>
//       </SliderWrapper>
//     </SliderContainer>
//   )
// }
// export default props => (
//   <StaticQuery
//     query={graphql`
//       query silderList {
//         allMdx(
//           limit: 3
//           filter: { frontmatter: { type: { eq: "work" } } }
//           sort: { fields: [frontmatter___date], order: DESC }
//         ) {
//           edges {
//             node {
//               id
//               fields {
//                 slug
//               }
//               frontmatter {
//                 title
//                 image {
//                   childImageSharp {
//                     fluid(maxWidth: 1200, quality: 100) {
//                       ...GatsbyImageSharpFluid
//                       src
//                     }
//                   }
//                 }
//               }
//             }
//           }
//         }
//       }
//     `}
//     render={data => <Slider data={data} {...props} />}
//   />
// )

// Slider.propTypes = {
//   data: PropTypes.shape({
//     allMdx: PropTypes.shape({
//       edges: PropTypes.array.isRequired,
//     }).isRequired,
//   }).isRequired,
// }
// const SliderContainer = styled.div``
// const SliderWrapper = styled.div`
//   ul {
//     display: flex;
//     width: 100%;
//     max-width: 1280px;
//     overflow: hidden;
//     position: relative;
//   }
// `
// const SliderContent = styled.div`
//   display: flex;
//   justify-content: center;
//   padding-top: 190px;
// `
// const SliderImage = styled.li`
//   .img {
//     width: 1280px;
//     height: 680px;
//     background-size: cover;
//     background-position: center;
//     transition: ease 0.4s;
//     filter: grayscale(100%);
//     &:hover {
//       filter: grayscale(0%);
//     }
//   }
// `
// const SliderTitle = styled.div`
//   padding: 380px 5%;
//   h1 {
//     color: white;
//     font-size: 72px;
//     position: absolute;
//     transition: ease 0.4s;
//     text-transform: uppercase;
//     @media screen and (min-width: 800px) {
//       width: 600px;
//     }
//     ${media.thone`font-size: 52px; `}
//     ${media.desktop`width: 80vw; `}
//   }
// `
// const SliderNavigation = styled.div`
//   display: flex;
//   width: 100%;
//   max-width: 1280px;
//   justify-content: space-between;
//   padding-top: 21px;
//   ${media.desktop`padding: 21px 4%; `}
// `
// const SliderArrows = styled.div`
//   display: flex;
//   h4 {
//     font-size: 16px;
//     transition: ease 0.4s;
//     cursor: pointer;
//     &:nth-child(1) {
//       padding-right: 31px;
//     }
//     &:hover {
//       color: #f64150;
//     }
//   }
// `
// const SliderIndicator = styled.div`
//   display: flex;
//   h4 {
//     font-size: 16px;
//   }
//   ul {
//     position: relative;
//     overflow: hidden;
//     width: 10px;
//     li {
//       width: 10px;
//       display: flex;
//       position: absolute;
//       opacity: 0;
//       &:nth-child(1) {
//         opacity: 1;
//       }
//     }
//   }
// `
// const SliderCategory = styled.h4``
