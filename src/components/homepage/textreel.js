import React, { useEffect } from 'react'
import styled from 'styled-components'
import { Controller, Scene } from 'react-scrollmagic'
import { Tween } from 'react-gsap'
import media from '../../utils/media'
import PropTypes from 'prop-types'
import { useInView } from 'react-intersection-observer'
import { useAnimation, motion } from 'framer-motion'

const TextReel = ({ text, triggerElement, id }) => {
  const n = 15
  const [ref, inView] = useInView({
    threshold: 0,
    triggerOnce: true,
    rootMargin: '-300px',
  })
  const animation = useAnimation()

  useEffect(() => {
    if (inView) {
      animation.start('visible')
    }
  }, [inView, animation])
  return (
    <TextReelWrapper
      ref={ref}
      id='trig'
      animate={animation}
      initial='hidden'
      variants={{
        visible: {
          opacity: 1,
          y: 0,
          transition: {
            delay: 0.2,
            duration: 0.5,
            ease: [0.6, 0.05, -0.01, 0.9],
          },
        },
        hidden: {
          opacity: 0,
          y: 83,
        },
      }}
    >
      <Controller>
        <Scene triggerElement='#trig' duration={2000} offset={-800}>
          <Tween to={{ x: -1000 }} ease='power2.easeInOut'>
            <div className='textReel'>
              {[...Array(n)].map((e, i) => (
                <h1 key={i}>{text}</h1>
              ))}
            </div>
          </Tween>
        </Scene>
        <Scene triggerElement='#trig' duration={2000} offset={-800}>
          <Tween from={{ x: -1000 }} ease='power2.easeInOut'>
            <div className='textReel2'>
              {[...Array(n)].map((e, i) => (
                <h1 key={i}>{text}</h1>
              ))}
            </div>
          </Tween>
        </Scene>
      </Controller>
    </TextReelWrapper>
  )
}

export default TextReel

TextReel.propTypes = {
  text: PropTypes.string,
  id: PropTypes.string,
  triggerElement: PropTypes.string,
}

const TextReelWrapper = styled(motion.div)`
  overflow: hidden;
  .textReel {
    width: 100%;
    display: flex;

    margin-left: -20px;
  }
  .textReel2 {
    width: 100%;
    display: flex;

    margin-left: -200px;
  }

  h1 {
    font-size: 144px;
    ${media.tablet`font-size: 92px;`}
    &:nth-of-type(2),
    :nth-of-type(4),
    :nth-of-type(6) {
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
    text-transform: uppercase;
  }
`
