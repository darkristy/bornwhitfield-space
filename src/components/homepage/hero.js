import React, { useRef, useEffect, useState } from 'react'
import styled from 'styled-components'
import media from '../../utils/media'
import Img from 'gatsby-image'
import { graphql, useStaticQuery } from 'gatsby'
import gsap, { Power3 } from 'gsap'

const Hero = () => {
  const [t1] = useState(gsap.timeline({ delay: 0.2 }))
  const data = useStaticQuery(graphql`
    query HeroImg {
      hero: file(relativePath: { eq: "misc/hero.png" }) {
        childImageSharp {
          fluid(maxWidth: 1000, quality: 100) {
            ...GatsbyImageSharpFluid
          }
        }
      }
    }
  `)
  //Hero Image Variables
  const image = data.hero.childImageSharp.fluid
  let imageStack = useRef(null)
  let headlineFirst = useRef(null)
  let headlineSecond = useRef(null)
  let container = useRef(null)

  let scrollRef = useRef(null)

  useEffect(() => {
    gsap.to(container, 0, { css: { visibility: 'visible' } })

    t1.from(imageStack, 1.2, { y: 1280, ease: Power3.easeOut }, '+=1')
      .from('.hero', 1, { scale: 1.6, ease: Power3.easeOut, delay: -1 })
      .from(
        [headlineFirst, headlineSecond],
        0.6,
        {
          y: 280,
          autoAlpha: 0,
          ease: Power3.easeInOut,
          stagger: {
            amount: 0.25,
          },
        },
        '-=.8'
      )
      .from(scrollRef, 0.6, {
        opacity: 0,
        ease: Power3.easeInOut,
      })
  }, [t1, imageStack, headlineSecond, headlineFirst, container])
  return (
    <div style={{ marginBottom: 200 }}>
      <HeroContainer ref={el => (container = el)}>
        <div style={{ overflow: 'hidden' }}>
          <HeroImg ref={el => (imageStack = el)}>
            <HeroBox />
            <Img
              className='hero'
              fluid={image}
              style={{ maxWidth: '100%', gridArea: 'overlap' }}
            />
          </HeroImg>
        </div>

        <HeroHeading>
          <div style={{ overflow: 'hidden' }}>
            <h1 ref={el => (headlineFirst = el)}>HELLO THERE.</h1>
            <h1 ref={el => (headlineSecond = el)}>HELLO THERE.</h1>
          </div>
        </HeroHeading>
      </HeroContainer>
      <HeroScrollWrapper ref={el => (scrollRef = el)}>
        <HeroScroll>
          <HeroScrollLineWrapper>
            <div className='scroll-line' />
          </HeroScrollLineWrapper>
          <HeroScrollTextWrapper>
            <p>scroll down</p>
          </HeroScrollTextWrapper>
        </HeroScroll>
      </HeroScrollWrapper>
    </div>
  )
}
export default Hero
const HeroImg = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-auto-rows: auto;
  grid-template-areas: 'overlap';
  padding-top: 100px;
`
const HeroHeading = styled.div`
  position: absolute;
  align-self: center;
  padding-top: 200px;
  ${media.tablet` padding-top: 280px; `}
  h1 {
    
    font-size: 144px;
    letter-spacing: 20px;
    ${media.bigDesktop`
    font-size: 124px;`}
    ${media.desktop`
    font-size: 96px;`}
    ${media.tablet` font-size: 80px; width: 300px; `}
   
    
  &:nth-of-type(2) {
      margin-top: -122px;
      ${media.bigDesktop` margin-top: -84px; `}
      ${media.tablet` margin-top: -188px; `}
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
  transition: ease .4s;
  }
`

const HeroBox = styled.div`
  background: #f1f1f1;
  width: 375px;
  height: 616px;
  grid-area: overlap;
  overflow: hidden;
`

const HeroContainer = styled.div`
  display: flex;
  position: relative;
  justify-content: center;
  margin-bottom: 200px;
  visibility: hidden;
`
export const SliderHeading = styled.div`
  display: flex;
  grid-column: 2 / span 12;
  ${media.tablet`grid-column: 2 / span 6;`}
  justify-content:center;
 
  h1 {
    font-size: 104px;
    transition: ease .4s;
    ${media.bigDesktop`font-size: 95px; `}
    ${media.tablet`font-size: 64px; `}
    ${media.thone`font-size: 49.7px; `}
    
    &:nth-of-type(2) {
      margin-top: -92px;
      ${media.bigDesktop` margin-top: -84px; `}
      ${media.tablet` margin-top: -58px; `}
      ${media.thone` margin-top: -42px; `}
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
  }
`
export const Seperator = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 18px;
  .seperator {
    background: ${props => props.theme.tietary};
    height: 1.8px;
    width: 140px;
  }
`
export const IntroTitle = styled.div`
 padding: 120px 0 180px;
 grid-column: 2 / span 12;
  ${media.tablet`grid-column: 2 / span 6;`}
 ${media.thone`
    font-size: 42px; padding: 80px 0 100px;`}
h1 {
    font-size: 96px;
    width: 100%;
    max-width: 800px;
    /* ${media.bigDesktop`
    font-size: 64px;`} */
    ${media.tablet`
    font-size: 64px;`}
    ${media.thone`
    font-size: 42px; `}
    ${media.phone`
    font-size: 32px;`}
    text-transform: uppercase;
  &:nth-child(2) {
      margin-top: -210px;
      
      ${media.tablet` margin-top: -142px; `}
      ${media.thone` margin-top: -90px; `}
      ${media.phone` margin-top: -68px; `}
      /* ${media.phone` display:none; `} */
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
    }
  transition: ease .4s;
  }
`

export const SliderButton = styled.div`
  padding: 12px 0;
  margin-top: 28px;
  width: 100%;
  max-width: 128px;
  height: 56;
  border: 1px solid ${props => props.theme.tietary};
  cursor: pointer;
  h4 {
    text-transform: uppercase;
    text-align: center;
  }
  transition: all 0.4s ease;
  &:hover {
    transition: all 0.4s ease;
    background: ${props => props.theme.tietary};
    h4 {
      color: ${props => props.theme.primary} !important;
    }
    border: none;
  }
`
export const SliderButton2 = styled.div`
  padding: 12px 0;
  margin-top: 60px;
  width: 100%;
  max-width: 128px;
  height: 56;
  border: 1px solid ${props => props.theme.tietary};
  cursor: pointer;
  h4 {
    text-transform: uppercase;
    text-align: center;
  }
`
export const SliderButton3 = styled.div`
  padding: 12px 0;
  margin-top: 60px;
  width: 100%;
  max-width: 128px;
  height: 56;
  border: 1px solid ${props => props.theme.tietary};
  cursor: pointer;
  h4 {
    text-transform: uppercase;
    text-align: center;
  }
`
const HeroScroll = styled.div`
  margin-top: -210px;
  position: relative;
  z-index: 3;
  .scroll-line {
    width: 1px;
    height: 48px;
    background: ${props => props.theme.tietary};
  }
  p {
    font-size: 12px;
    text-transform: uppercase;
    color: ${props => props.theme.tietary};
    padding-top: 20px;
  }
`
const HeroScrollWrapper = styled.div`
  display: flex;
  justify-content: center;
`
const HeroScrollLineWrapper = styled.div`
  display: flex;
  justify-content: center;
`
const HeroScrollTextWrapper = styled.div`
  overflow: hidden;
`
