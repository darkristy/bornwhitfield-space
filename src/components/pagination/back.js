import React, { useState } from 'react'
import styled from 'styled-components'
import { motion } from 'framer-motion'
import AniLink from 'gatsby-plugin-transition-link/AniLink'

const Back = () => {
  const [hovered, setHovered] = useState(false)
  return (
    <BackButton
      onHoverStart={() => setHovered(!hovered)}
      onHoverEnd={() => setHovered(!hovered)}
      //   onMouseEnter={() => onCursor('hovered')}
      //   onMouseLeave={onCursor}
    >
      <AniLink
        cover
        direction='down'
        bg='black'
        to={() => typeof history !== 'undefined' && history.go(-1)}
      >
        <span className='back-label'>
          <h1>Back</h1>
        </span>

        <span className='arrow'>
          <motion.svg
            animate={{ x: hovered ? 22 : 0 }}
            transition={{ duration: 0.6, ease: [0.6, 0.05, -0.01, 0.9] }}
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 101 57'
          >
            <path
              d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
              fill='#000'
              fillRule='evenodd'
            />
          </motion.svg>
        </span>
      </AniLink>
    </BackButton>
  )
}

export default Back

const BackButton = styled(motion.div)`
  width: 55px;
  padding: 100px 0 20px 10%;
  .arrow {
    width: 83px;
    height: 80px;
    display: block;
    position: relative;
    overflow: hidden;
    display: flex;
    margin-left: auto;

    transform: rotate(180deg);
    svg {
      position: absolute;
      top: 16px;
      left: -29px;
      width: 88px;
      path {
        fill: transparent;
        stroke: ${props => props.theme.tietary};
      }
    }
  }
  .back-label {
    h1 {
      font-size: 2.8rem;
      width: fit-content;
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
      text-transform: uppercase;
    }
    &:before {
      width: 0;
      color: ${props => props.theme.tietary};
      -webkit-text-fill-color: ${props => props.theme.tietary};
      overflow: hidden;
      position: absolute;
      content: attr(data-text);
      transition: all 1s cubic-bezier(0.84, 0, 0.08, 0.99);
    }
    &:hover {
      &:before {
        width: 100%;
      }
    }
  }
`
