import React, { useState, useContext } from 'react'
import styled from 'styled-components'
import { Row, Cell, Provider } from 'griding'
import { css } from 'emotion'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import PropTypes from 'prop-types'
import { motion } from 'framer-motion'
import Context from '../../context/context'

const PaginationStyles = styled.div`
  a[disabled] {
    /* opacity: 0.5;
    pointer-events: none;
    text-decoration: line-through; */
    display: none;
  }
`

const PagePagination = ({ totalCount, currentPage = 1, pathPrefix }) => {
  const totalPages = Math.ceil(totalCount / 10)
  const nextPage = currentPage + 1
  const prevPage = currentPage - 1

  const [hoveredLeft, setHoveredLeft] = useState(false)
  const [hoveredRight, setHoveredRight] = useState(false)

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }
  return (
    <PaginationStyles>
      <PaginationContianer
        className={css`
          padding: 0 10%;
        `}
        // ref={el => (arrowRef = el)}
      >
        <Provider>
          <Row>
            <Cell xs={12}>
              <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
                <PageLink
                  onMouseEnter={() => onCursor('pointer')}
                  onMouseLeave={onCursor}
                  onHoverStart={() => setHoveredLeft(!hoveredLeft)}
                  onHoverEnd={() => setHoveredLeft(!hoveredLeft)}
                  left
                >
                  <div style={{ maxWidth: '192px' }}>
                    <AniLink
                      cover
                      direction='down'
                      bg='black'
                      disabled={prevPage <= 0 ? true : null}
                      to={`${pathPrefix}${prevPage}`}
                    >
                      <span className='label'>
                        <h1>Prev</h1>
                      </span>

                      <span className='arrow' style={{ top: 10 }}>
                        <motion.svg
                          animate={{ x: hoveredLeft ? 48 : 0 }}
                          transition={{
                            duration: 0.6,
                            ease: [0.6, 0.05, -0.01, 0.9],
                          }}
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 101 57'
                        >
                          <path
                            d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                            fill='#000'
                            fillRule='evenodd'
                          />
                        </motion.svg>
                      </span>
                    </AniLink>
                  </div>
                </PageLink>
              </div>
            </Cell>
          </Row>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <PageLink
              onHoverStart={() => setHoveredRight(!hoveredRight)}
              onHoverEnd={() => setHoveredRight(!hoveredRight)}
              onMouseEnter={() => onCursor('pointer')}
              onMouseLeave={onCursor}
            >
              <AniLink
                cover
                direction='down'
                bg='black'
                disabled={nextPage > totalPages ? true : null}
                to={nextPage > totalPages ? null : `${pathPrefix}${nextPage}`}
              >
                <span className='label'>
                  <h1>Prev</h1>
                </span>

                <span className='arrow'>
                  <motion.svg
                    animate={{ x: hoveredRight ? 48 : 0 }}
                    transition={{
                      duration: 0.6,
                      ease: [0.6, 0.05, -0.01, 0.9],
                    }}
                    xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 101 57'
                  >
                    <path
                      d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                      fill='#000'
                      fillRule='evenodd'
                    />
                  </motion.svg>
                </span>
              </AniLink>
            </PageLink>
          </div>
        </Provider>
      </PaginationContianer>
    </PaginationStyles>
  )
}

export default PagePagination

PagePagination.propTypes = {
  totalCount: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
  currentPage: PropTypes.any.isRequired,
}

const PageLink = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  border-radius: 4px;

  transition: 150ms all ease-in;
  &:hover {
    transition: 150ms all ease-in;
  }

  .label {
    h1 {
      font-size: 4rem;
      width: fit-content;
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
      text-transform: uppercase;
    }
  }
  .arrow {
    width: 119.58px;
    height: 80px;
    display: block;
    position: relative;
    overflow: hidden;
    display: flex;
    margin-left: auto;
    ${props =>
      props.left &&
      css`
        transform: rotate(180deg);
      `}

    svg {
      position: absolute;
      top: 16px;
      left: -49px;
      width: 108px;
      path {
        fill: transparent;
        stroke: ${props => props.theme.tietary};
      }
    }
  }
`

const PaginationContianer = styled.div`
  margin: 10% 0;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
`
