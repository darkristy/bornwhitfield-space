import React, { useState, useContext } from 'react'
import styled, { css } from 'styled-components'
import { Row, Cell, Provider } from 'griding'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
import PropTypes from 'prop-types'
import { motion } from 'framer-motion'
import Context from '../../context/context'

const PostPagination = ({ prev, next, pathPrefix }) => {
  const [hoveredLeft, setHoveredLeft] = useState(false)
  const [hoveredRight, setHoveredRight] = useState(false)

  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }
  return (
    <PaginationContianer

    // ref={el => (arrowRef = el)}
    >
      <Provider>
        <Row>
          <Cell xs={12}>
            {prev && (
              <div style={{ display: 'flex', justifyContent: 'flex-start' }}>
                <PageLink
                  onMouseEnter={() => onCursor('pointer')}
                  onMouseLeave={onCursor}
                  onHoverStart={() => setHoveredLeft(!hoveredLeft)}
                  onHoverEnd={() => setHoveredLeft(!hoveredLeft)}
                  left
                >
                  <div style={{ maxWidth: '192px' }}>
                    <AniLink
                      cover
                      direction='down'
                      bg='black'
                      to={`${pathPrefix}${prev.node.fields.slug}`}
                    >
                      <span className='label'>
                        <h1>Prev</h1>
                      </span>

                      <span className='arrow' style={{ top: 10 }}>
                        <motion.svg
                          animate={{ x: hoveredLeft ? 48 : 0 }}
                          transition={{
                            duration: 0.6,
                            ease: [0.6, 0.05, -0.01, 0.9],
                          }}
                          xmlns='http://www.w3.org/2000/svg'
                          viewBox='0 0 101 57'
                        >
                          <path
                            d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                            fill='#000'
                            fillRule='evenodd'
                          />
                        </motion.svg>
                      </span>
                    </AniLink>
                  </div>
                </PageLink>
              </div>
            )}
          </Cell>
        </Row>
        {next && (
          <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
            <PageLink
              onHoverStart={() => setHoveredRight(!hoveredRight)}
              onHoverEnd={() => setHoveredRight(!hoveredRight)}
              onMouseEnter={() => onCursor('pointer')}
              onMouseLeave={onCursor}
            >
              <AniLink
                cover
                direction='down'
                bg='black'
                to={`${pathPrefix}${next.node.fields.slug}`}
              >
                <span className='label'>
                  <h1>Prev</h1>
                </span>

                <span className='arrow'>
                  <motion.svg
                    animate={{ x: hoveredRight ? 48 : 0 }}
                    transition={{
                      duration: 0.6,
                      ease: [0.6, 0.05, -0.01, 0.9],
                    }}
                    xmlns='http://www.w3.org/2000/svg'
                    viewBox='0 0 101 57'
                  >
                    <path
                      d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                      fill='#000'
                      fillRule='evenodd'
                    />
                  </motion.svg>
                </span>
              </AniLink>
            </PageLink>
          </div>
        )}
      </Provider>
    </PaginationContianer>
  )
}

export default PostPagination

PostPagination.propTypes = {
  prev: PropTypes.any.isRequired,
  next: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
}

const PageLink = styled(motion.div)`
  display: flex;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  .label {
    h1 {
      font-size: 4rem;
      width: fit-content;
      -webkit-text-fill-color: transparent;
      -webkit-text-stroke-width: 1px;
      -webkit-text-stroke-color: ${props => props.theme.tietary};
      text-transform: uppercase;
    }
  }
  .arrow {
    width: 119.58px;
    height: 80px;
    display: block;
    position: relative;
    overflow: hidden;
    display: flex;
    margin-left: auto;
    ${props =>
      props.left &&
      css`
        transform: rotate(180deg);
      `}

    svg {
      position: absolute;
      top: 16px;
      left: -49px;
      width: 108px;
      path {
        fill: transparent;
        stroke: ${props => props.theme.tietary};
      }
    }
  }
`

const PaginationContianer = styled.div`
  margin: 10% 0;
  font-family: Silka, monospace;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
`
