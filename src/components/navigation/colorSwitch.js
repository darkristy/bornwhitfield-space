import React, { useContext } from 'react'
import Switch from '../icons/switch'
import styled from 'styled-components'
// import { onSwitchIn, onSwitchOut } from '../../utils/cursor'

import Context from '../../context/context'
const ColorSwitch = ({ onCursor }) => {
  const { dispatch, state } = useContext(Context)

  const toggleTheme = () => {
    if (state.currentTheme === 'dark') {
      dispatch({ type: 'TOGGLE_DARK_MODE', theme: 'light' })
    } else {
      dispatch({ type: 'TOGGLE_DARK_MODE', theme: 'dark' })
    }
  }

  return (
    <Wrapper
      onClick={toggleTheme}
      onMouseEnter={() => onCursor('toggle')}
      onMouseLeave={onCursor}
      flip={state.currentTheme === 'light'}
    >
      <Switch />
    </Wrapper>
  )
}

export default ColorSwitch

const Wrapper = styled.button`
  background: none;
  transform: scaleX(${props => (props.flip ? '1' : '-1')});
  svg {
    fill: ${props => props.theme.tietary};
  }
`
