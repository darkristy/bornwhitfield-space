import React, { useContext, useState, useEffect, useRef } from 'react'
import { window } from 'browser-monads'
import AniLink from 'gatsby-plugin-transition-link/AniLink'

import PropTypes from 'prop-types'
import { motion } from 'framer-motion'
// import { onLinkIn, onLinkOut } from '../../utils/cursor'

import Context from '../../context/context'

const Routes = ({ href, label }) => {
  const { dispatch, state } = useContext(Context)

  const onCursor = cursorType => {
    cursorType =
      (state.cursorStyles.includes(cursorType) && cursorType) || false
    dispatch({ type: 'CURSOR_TYPE', cursorType: cursorType })
  }

  const [hasActive, setHasActive] = useState(true)

  useEffect(() => {
    if (window.location.href.indexOf(href) > 0) {
      setHasActive(false)
    } else {
      setHasActive(true)
    }
  }, [hasActive, href])
  return (
    <motion.li>
      <AniLink
        cover
        direction='down'
        bg='black'
        to={href}
        className={
          window.location.href.indexOf(href) > 0
            ? 'nav__item-link active'
            : 'nav__item-link'
        }
      >
        <motion.div
          initial={{ x: -140 }}
          className='link'
          whileHover={{
            x: hasActive ? -40 : -140,
            transition: {
              duration: 0.4,
              ease: [0.6, 0.05, -0.01, 0.9],
            },
          }}
        >
          <span className='arrow'>
            <motion.svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 101 57'>
              <path
                d='M33 34H0V24h81.429L66 7.884 73.548 0l19.877 20.763.027-.029L101 28.618 73.829 57l-7.548-7.884L80.753 34H33z'
                fill='#000'
                fillRule='evenodd'
              ></path>
            </motion.svg>
          </span>
          <span
            className='route'
            data-text={label}
            onMouseEnter={hasActive ? () => onCursor('hovered') : onCursor}
            onMouseLeave={onCursor}
          >
            {label}
          </span>
        </motion.div>
      </AniLink>
    </motion.li>
  )
}

export default Routes

Routes.propTypes = {
  href: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onCursor: PropTypes.any,
}
