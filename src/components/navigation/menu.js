import React, { useEffect, useRef } from 'react'
import Routes from './routes'
import styled from 'styled-components'
import gsap from 'gsap'
import PropTypes from 'prop-types'
import * as pallete from '../../utils/variables'
import MainFooter from '../footercomponent/footer'

const links = [
  { key: '', href: '/works', label: 'Work' },
  // { key: '', href: '/collective', label: 'Collective' },
  { key: '', href: '/blog', label: 'Blog' },

  { key: '', href: '/about', label: 'About' },
].map(link => {
  link.key = `nav-link-${link.href}-${link.label}`
  return link
})

const staggerRevealClose = (node1, node2) => {
  gsap.to([node1, node2], {
    duration: 0.8,
    height: 0,
    ease: 'power3.inOut',
  })
}
const staggerReveal = (node1, node2) => {
  gsap.from([node1, node2], {
    duration: 0.8,
    height: 0,
    transformOrigin: 'right top',
    skewY: 2,
    ease: 'power3.inOut',
  })
}
// const staggerText = (node1, node2, node3) => {
//   gsap.from([node1, node2, node3], {
//     duration: 0.8,
//     x: 100,
//     autoAlpha: 0,
//     delay: 0.2,
//     ease: 'power3.inOut',
//     stagger: {
//       amount: 0.3,
//     },
//   })
// }

const Menu = ({ state }) => {
  let menu = useRef(null)
  let reveal1 = useRef(null)
  let reveal2 = useRef(null)
  // let line1 = useRef(null)
  // let line2 = useRef(null)
  // let line3 = useRef(null)

  useEffect(() => {
    if (state.clicked === false) {
      //Initial Animation
      staggerRevealClose(reveal2, reveal1)
      gsap.to(menu, { duration: 1, css: { display: 'none' } })
    } else if (
      state.clicked === true ||
      (state.clicked === true && state.initial === null)
    ) {
      gsap.to(menu, { duration: 0, css: { display: 'block' } })
      gsap.to([reveal1, reveal2], {
        duration: 0,
        opacity: 1,
        height: '100%',
      })
      staggerReveal(reveal1, reveal2)
      // staggerText(line1, line2, line3)
    }
  }, [state])

  return (
    <MenuContainer ref={el => (menu = el)}>
      <SecondaryBackgroundColor ref={el => (reveal1 = el)}>
        <OtherLayer ref={el => (reveal2 = el)}>
          <MenuWrapper>
            <ul>
              {links.map(({ key, href, label }) => (
                <Routes key={key} href={href} label={label} />
              ))}
            </ul>
            <MainFooter />
          </MenuWrapper>
        </OtherLayer>
      </SecondaryBackgroundColor>
    </MenuContainer>
  )
}

export default Menu

Menu.propTypes = {
  state: PropTypes.any.isRequired,
}

const MenuWrapper = styled.div`
  padding: 0 10%;
  height: 100%;
  width: 100%;

  position: relative;
  top: 200px;
  ul {
    margin: 0;
    padding-bottom: 200px;

    li {
      list-style: none;
      width: fit-content;
      font-size: 124px;
      line-height: 124px;
      text-transform: uppercase;
      height: 124px;
      font-family: ${pallete.PRIMARY_FONT};
      overflow: hidden;
      span {
        position: relative;

        &:before {
          width: 0;
          color: ${props => props.theme.tietary};
          -webkit-text-fill-color: ${props => props.theme.tietary};
          overflow: hidden;
          position: absolute;
          content: attr(data-text);
          transition: all 1s cubic-bezier(0.84, 0, 0.08, 0.99);
        }
        &:hover {
          &:before {
            width: 100%;
          }
        }
      }
      .link {
        position: relative;

        -webkit-text-fill-color: transparent;
        -webkit-text-stroke-width: 0.8px;
        -webkit-text-stroke-color: ${props => props.theme.tietary};
      }
      .arrow {
        height: 138px;
        margin-right: 8px;
      }

      .active {
        .link {
          -webkit-text-fill-color: ${props => props.theme.tietary};
          -webkit-text-stroke-width: 0.6px;
        }
      }
    }
    svg {
      width: 140px;
      path {
        fill: ${props => props.theme.tietary};
      }
    }
  }
`
const SecondaryBackgroundColor = styled.div`
  background: ${props => props.theme.background};
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  position: fixed;
  height: 100%;
  width: 100%;
  z-index: -1;
`

const OtherLayer = styled.div`
  position: relative;
  background: ${props => props.theme.background};
  height: 100%;
  overflow: hidden;
`
const MenuContainer = styled.div`
  display: none;
  z-index: 2;
  top: 0;
  left: 0;
  right: 0;
  position: fixed;
  height: 100%;
  width: 100%;
`
