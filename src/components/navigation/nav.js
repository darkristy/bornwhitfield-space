import React, { useState, useRef } from 'react'
import styled from 'styled-components'
import ColorSwitch from './colorSwitch'
import Icon from '../icons/logo'
import Menu from './menu'
import { css } from 'emotion'
import AniLink from 'gatsby-plugin-transition-link/AniLink'
// import { onLinkIn, onLinkOut } from '../../utils/cursor'
import { motion } from 'framer-motion'
import PropTypes from 'prop-types'
import useElementPosition from '../../hooks/useElementPosition'

const Nav = ({ onCursor, togglePosition, setTogglePosition }) => {
  const toggle = useRef(null)
  const position = useElementPosition(toggle)

  const toggleHover = () => {
    onCursor('locked')
    setTogglePosition({ x: position.x, y: position.y + 72 })
  }

  const [state, setState] = useState({
    initial: false,
    clicked: null,
    menuName: 'Menu',
  })

  const [isOpen, setIsOpen] = useState(false)

  const [disabled, setDisabled] = useState(false)

  const handleMenu = () => {
    disabledMenu()
    if (state.initial === false) {
      setState({
        initial: null,
        clicked: true,
      })
      setIsOpen(!isOpen)
    } else if (state.clicked === true) {
      setState({
        clicked: !state.clicked,
      })
      setIsOpen(!isOpen)
    } else if (state.clicked === false) {
      setState({
        clicked: !state.clicked,
      })
      setIsOpen(!isOpen)
    }
  }
  const disabledMenu = () => {
    setDisabled(!disabled)
    setTimeout(() => {
      setDisabled(false)
    }, 800)
  }
  return (
    <>
      <Navi
        animate={{ y: 0, opacity: 1 }}
        initial={{ y: -72, opacity: 0 }}
        transition={{ duration: 1, ease: [0.6, 0.05, -0.01, 0.9], delay: 1 }}
      >
        <ul className='nav__item'>
          <li className='nav__item--left'>
            <AniLink cover direction='down' bg='black' to='/'>
              <Logo
                onMouseEnter={() => onCursor('hovered')}
                onMouseLeave={onCursor}
              >
                <Icon />
              </Logo>
            </AniLink>
          </li>
          <li
            className={css`
              padding-right: 20px;
            `}
          >
            <ColorSwitch onCursor={onCursor} />
          </li>

          <Toggle
            disabled={disabled}
            open={isOpen}
            onClick={handleMenu}
            onMouseEnter={toggleHover}
            onMouseLeave={onCursor}
            ref={toggle}
          >
            <span></span>
            <span></span>
          </Toggle>
        </ul>
      </Navi>
      <Menu state={state} onCursor={onCursor} />
    </>
  )
}

export default Nav

Nav.propTypes = {
  onCursor: PropTypes.any,
  togglePosition: PropTypes.any,
  setTogglePosition: PropTypes.any,
}

const Navi = styled(motion.nav)`
  padding: 0 10%;
  margin: 10px 0;
  border-right: none;
  width: 100%;
  height: 60px;
  position: relative;
  z-index: 4;
  .nav__item {
    display: flex;
    align-items: center;
    flex-direction: row;
    &-link {
    }
    &--left {
      margin-right: auto;
    }
  }
`

const Logo = styled.div`
  svg {
    fill: ${props => props.theme.tietary};
    transition: all 0.4s ease;
  }
  transition: all 0.4s ease;
  &:hover {
    transform: scale(0.9);
    transition: all 0.4s ease;
  }
`

const Toggle = styled.button`
  transition: transform 0.3s;
  background: none;
  top: 0;
  bottom: 0;
  width: 1.5rem;
  height: 60px;
  span {
    transition: all 0.3s;
    display: block;
    background: ${props => props.theme.text};
    width: 100%;
    height: 2px;
  }
  span:first-of-type {
    transform: rotate(${props => (props.open ? '45deg' : '0')})
      translateY(${props => (props.open ? '0' : '.35rem')});
  }
  span:nth-of-type(2n) {
    transform: rotate(${props => (props.open ? '-45deg' : '0')})
      translateY(${props => (props.open ? '0' : '-.35rem')});
    position: relative;
    bottom: ${props => (props.open ? '2px' : '0')};
  }
`
