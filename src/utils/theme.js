import * as pallete from './variables'

// export default {
//   body: `${pallete.DARK_PRIMARY_COLOR}`,
//   headingText: `${pallete.DARK_TIETARY_COLOR}`,
//   secondHeadingText: `${pallete.DARK_TIETARY_COLOR}`,
//   text: `gray`,
//   link: `${pallete.DARK_TIETARY_COLOR}`,
//   border: `${pallete.DARK_SECONDARY_COLOR}`,
//   active: `${pallete.DARK_SECONDARY_COLOR}`,
//   activeColor: `gray`,
//   hover: `${pallete.DARK_SECONDARY_COLOR}`,
//   languageText: `${pallete.DARK_TIETARY_COLOR}`,
//   tableTitle: `${pallete.DARK_TIETARY_COLOR}`,
//   strong: `${pallete.DARK_TIETARY_COLOR}`,
//   blockquote: `${pallete.DARK_TIETARY_COLOR}`,
//   coral: `${pallete.CORAL}`,
// }

export const darkTheme = {
  text: `${pallete.DARK_TIETARY_COLOR}`,
  background: `${pallete.DARK_PRIMARY_COLOR}`,
  primary: `${pallete.DARK_PRIMARY_COLOR}`,
  secondary: `${pallete.DARK_SECONDARY_COLOR}`,
  tietary: `${pallete.DARK_TIETARY_COLOR}`,
  accent: `${pallete.CORAL}`,
}

export const lightTheme = {
  text: `${pallete.LIGHT_TIETARY_COLOR}`,
  background: `${pallete.LIGHT_PRIMARY_COLOR}`,
  primary: `${pallete.LIGHT_PRIMARY_COLOR}`,
  secondary: `${pallete.LIGHT_SECONDARY_COLOR}`,
  tietary: `${pallete.LIGHT_TIETARY_COLOR}`,
  accent: `${pallete.CORAL}`,
}
