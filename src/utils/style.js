import styled from 'styled-components'
import * as pallete from './variables'

export const Container = styled.div`
  transition: transform 0.8s;
`
export const App = styled.div`
  .App {
    position: relative;
    z-index: 2;
    background: ${props => props.theme.colors.primary};
  }
`
export const SocialLink = styled.div`
  display: flex;
  .Instagram {
    transition: 150ms all ease-in;
    &:hover {
      transition: 150ms all ease-in;
      color: ${pallete.ORANGE};
    }
  }

  .Dribbble {
    transition: 150ms all ease-in;
    &:hover {
      transition: 150ms all ease-in;
      color: ${pallete.PINK};
    }
  }

  .Behance {
    transition: 150ms all ease-in;
    &:hover {
      transition: 150ms all ease-in;
      color: ${pallete.SEA_BLUE};
    }
  }

  .Twitter {
    transition: 150ms all ease-in;
    &:hover {
      transition: 150ms all ease-in;
      color: ${pallete.LIGHT_BLUE};
    }
  }

  .Github {
    transition: 150ms all ease-in;
    &:hover {
      transition: 150ms all ease-in;
      color: ${pallete.PURPLE};
    }
  }
`
