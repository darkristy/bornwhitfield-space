import * as pallete from './variables'
import media from './media'
import styled, { createGlobalStyle } from 'styled-components'
import { motion } from 'framer-motion'

export const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    box-sizing: border-box;
    cursor: none;
    ::-webkit-scrollbar {
      width: 0px; /* Remove scrollbar space */
      background: transparent; /* Optional: just make scrollbar invisible */
    }
  }

  /* .smooth-scroll-wrapper {
    position: fixed;
    z-index: 2;
    top: 0;
    left: 0;
    height: 100%;
    width: 100%;
    overflow: hidden;
  } */

  html {
    color: white;
    font-kerning: normal;
    overflow-x: hidden;
  }

  body {
    background: ${props => props.theme.primary};
    display: block;
  }

  a {
    text-transform: uppercase;
    text-decoration: none !important;
    transition: all 0.3s ease 0s;
    color: ${props => props.theme.text};
    &:hover {
      transition: 0.3s all ease;
    }
  }

  nav span {
    font-family: ${pallete.SECONDARY_FONT};
  }

  ul {
    list-style: none;
  }

  em {
    margin-top: -32px;
    display: flex;
    justify-content: center;
    text-align: center;
    font-size: 70%;
    padding-bottom: 20px;
    font-family: ${pallete.SECONDARY_FONT};
  }

  h1 {
    color: ${props => props.theme.tietary};
    font-size: calc(2.875rem + ((1vw - 4.8px) * 2.5));
    font-family: ${pallete.PRIMARY_FONT};
    ${media.tablet`font-size:calc(2.4rem + ((1vw - 4.8px) * 2.5));`};
    font-weight: normal;
  }

  h2 {
    color: ${props => props.theme.tietary};
    font-family: ${pallete.PRIMARY_FONT};
    font-size: calc(1.625rem + ((1vw - 6.76px) * 2.4116));
    font-weight: normal;
  }

  h3 {
    color: ${props => props.theme.tietary};
    font-family: ${pallete.SECONDARY_FONT};
    font-size: 32px;
    font-weight: 200;
  }

  h4 {
    color: ${props => props.theme.tietary};
    font-weight: 200;
    font-size: calc(0.75rem + ((1vw - 4.8px) * 0.1389));
    font-family: ${pallete.SECONDARY_FONT};
  }

  h5 {
    color: ${props => props.theme.tietary};
    font-weight: 200;
    font-size: 12px;
    font-family: ${pallete.SECONDARY_FONT};
  }

  p {
    font-family: ${pallete.SECONDARY_FONT};
    font-size: 18px;
    color: gray;

    text-transform: initial;
    ${media.tablet`font-size: 13.8px;`}
  }
`

/* export const Container = styled.div`
   display: grid;
 height: 100%;
grid-template-columns: 1fr repeat(12, minmax(auto, 4.2rem)) 1fr;
  grid-template-rows: auto;
  grid-gap: 0 2rem;
  ${media.tablet`grid-template-columns: 2rem repeat(6, 1fr) 2rem; grid-gap: 0 1rem;`}
  ${media.thone`grid-template-columns: 1rem repeat(6, 1fr) 1rem; ;`}
 ` */

export const Cursor = styled(motion.div)`
  position: absolute;
  top: 200px;
  left: 200px;
  height: 12px;
  width: 12px;
  background: ${props => props.theme.tietary};
  border-radius: 100%;
  transform: translate(-50%, -50%);
  transition: all 0.2s ease-out;
  transition-property: width, height, border;
  will-change: width, height, transform, border;
  pointer-events: none;
  z-index: 9999;

  &.hovered {
    width: 56px;
    height: 56px;
    background: transparent !important;
    border: 4px solid ${props => props.theme.tietary};
  }
  &.pointer {
    border: none;
    width: 56px;
    height: 56px;
    background: ${props => props.theme.tietary} !important;
  }
  &.toggle {
    background: ${props => props.theme.accent} !important;
    height: 12px;
    width: 12px;
    border: none;
  }
  &.locked {
    width: 56px;
    height: 56px;
    background: transparent !important;
    border: 4px solid ${props => props.theme.tietary};
    top: ${props => props.theme.top} !important;
    left: ${props => props.theme.left} !important;
  }
  &.hide {
    display: none;
  }
  &.b-button {
    border: 4px solid ${props => props.theme.tietary} !important;
  }
  &.w-button {
    border: 4px solid ${props => props.theme.primary} !important;
  }
`
