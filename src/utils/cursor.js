import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'

import styled from 'styled-components'

export const onLinkIn = () => {
  const element = document.querySelector('.cursor')
  element.classList.add('hovered')
}

export const onLinkOut = () => {
  const element = document.querySelector('.cursor')
  element.classList.remove('hovered')
}
export const onSwitchIn = () => {
  const element = document.querySelector('.cursor')
  element.classList.add('toggle')
}

export const onSwitchOut = () => {
  const element = document.querySelector('.cursor')
  element.classList.remove('toggle')
}

const CustomCursor = () => {
  const [mousePosition, setMousePosition] = useState({
    x: 400,
    y: 400,
  })

  const onMouseMove = event => {
    const { pageX: x, pageY: y } = event
    setMousePosition({ x, y })
  }

  useEffect(() => {
    document.addEventListener('mousemove', onMouseMove)

    return () => {
      document.removeEventListener('mousemove', onMouseMove)
    }
  }, [])

  return (
    <CursorWrapper>
      <div
        className='cursor'
        style={{ left: `${mousePosition.x}px`, top: `${mousePosition.y}px` }}
      />
    </CursorWrapper>
  )
}

CustomCursor.propTypes = {}

export default CustomCursor

const CursorWrapper = styled.div`
  .cursor {
    position: absolute;
    top: 200px;
    left: 200px;
    height: 10px;
    width: 10px;
    background: ${props => props.theme.tietary};
    border-radius: 100%;
    transform: translate(-50%, -50%);
    transition: all 0.1s ease-in-out;
    transition-property: width, height, border;
    will-change: width, height, transform, border;
    pointer-events: none;
    z-index: 9999;

    &.hovered {
      background: transparent !important;
      width: 56px;
      height: 56px;
      border: 2px solid ${props => props.theme.tietary};
    }
    &.pointer {
      border: 2px solid ${props => props.theme.tietary};
    }
    &.toggle {
      background: ${props => props.theme.accent};
    }
  }
`
