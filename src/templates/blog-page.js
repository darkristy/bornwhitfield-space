import React, { useEffect, useRef, useState } from 'react'
import { graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import styled from 'styled-components'
import { MDXProvider } from '@mdx-js/react'
import PropTypes from 'prop-types'

//Component Imports
import Layout from '../components/layout'
import PostHead from '../components/blog/post-head'
import SEO from '../components/seo'
import BlogFooter from '../components/footercomponent/blogfooter'
import PostPagination from '../components/pagination/post-pagination'

//Utillites
import { css } from 'emotion'
import gsap from 'gsap/dist/gsap'
import * as pallete from '../utils/variables'
import media from '../utils/media'

export default function BlogTemplate({ data, pageContext }) {
  const shortcodes = {}

  //Call in the data props
  const { mdx } = data
  const { prev, next, pathPrefix } = pageContext
  const { frontmatter, body } = mdx

  //Elements References
  let blogRef = useRef(null)
  let blogWrapRef = useRef(null)

  // GSAP Anime
  const [t1] = useState(gsap.timeline({ delay: 0.1 }))
  useEffect(() => {
    t1.from(blogWrapRef, 0.8, {
      delay: 0.2,
      autoAlpha: 0,
    })
      .from(
        blogRef,
        0.8,
        {
          opacity: 0,
          delay: 0.2,
          y: -24,
          ease: 'power3.InOut',
          stagger: {
            amount: 0.9,
          },
        },
        '-=.2'
      )
      .from(
        '.p1',
        0.8,
        {
          opacity: 0,
          delay: 0.2,
          x: -24,
          ease: 'power3.InOut',
          stagger: {
            amount: 0.9,
          },
        },
        '-=.2'
      )
  }, [t1, blogRef, blogWrapRef])

  //Meat of the blog page
  return (
    <Layout>
      <SEO title={frontmatter.title} description={frontmatter.seoDesc} />

      <div
        className={css`
          visibility: hidden;
          padding-top: 200px;
        `}
        ref={el => (blogWrapRef = el)}
      >
        <PostHead
          image={frontmatter.image.childImageSharp.fluid.src}
          date={frontmatter.date}
          readtime={mdx.timeToRead}
          title={frontmatter.title}
        />
      </div>
      <div ref={el => (blogRef = el)}>
        <PostContent>
          <MDXProvider components={shortcodes}>
            <MDXRenderer>{body}</MDXRenderer>
          </MDXProvider>
          <PostPagination prev={prev} next={next} pathPrefix={pathPrefix} />
        </PostContent>

        <BlogFooter />
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogTemplate($slug: String) {
    mdx(fields: { collection: { eq: "post" }, slug: { eq: $slug } }) {
      body
      timeToRead
      frontmatter {
        categories
        tags
        title
        date(formatString: "MM/DD/YY")
        seoTitle
        seoDesc
        seoKeywords
        seoImage {
          childImageSharp {
            fluid(maxWidth: 1200, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
        image {
          childImageSharp {
            fluid(maxWidth: 1200, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
      }
    }
  }
`

BlogTemplate.propTypes = {
  pageContext: PropTypes.any.isRequired,
  pathPrefix: PropTypes.any.isRequired,
  data: PropTypes.shape({
    mdx: PropTypes.shape({
      body: PropTypes.any.isRequired,
      timeToRead: PropTypes.string.isRequired,
      frontmatter: PropTypes.shape({
        role: PropTypes.string.isRequired,
        categories: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        desc: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        seoTitle: PropTypes.string.isRequired,
        seoDesc: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
}

export const PostContent = styled.div`
  margin-top: 60px;
  max-width: 1280px;
  padding: 0 10%;
  margin: 0 auto;
  line-height: 40px;
  .Tweet {
    display: flex;
    justify-content: center;
  }
  .Video {
    padding: 10px 0;
    margin: 0 10%;
    ${media.tablet`margin: 0;`}
  }
  .language-text {
  }
  h2 {
    margin-bottom: 40px;
    text-transform: uppercase;
    font-weight: inherit;
  }
  a {
    color: gray;
    transition: all 0.3s ease 0s;
    text-transform: inherit;
    text-decoration: underline;
    &:hover {
      transition: 0.3s all ease;
      text-decoration: none;
      color: ${pallete.CORAL};
    }
  }
  strong {
    font-weight: 600;
  }
  b {
    font-weight: 600;
  }

  h3 {
    @media screen and (max-width: 767px) {
      font-size: 24px;
    }
  }
  p {
    margin-bottom: 60px;
  }
  img {
    width: 100%;
    border-radius: 4px;
    margin: 0 auto;
  }
  ul {
    margin-left: 20px;
    padding: 0;
    list-style-type: none;
    font-family: ${pallete.SECONDARY_FONT};
    font-size: 18px;
    ${media.tablet`font-size: 13.8px;`}
    li {
      margin-bottom: 15px;
      padding-left: 20px;
      line-height: 1.5;
      &:before {
        content: '-';

        position: absolute;
        margin-left: -20px;
      }
    }
  }

  blockquote {
    margin: 30px 0;
    font-style: oblique;
    border-left: 4px solid gray;
    padding-left: 20px;
    margin-left: 20px;
    p {
      line-height: 28px;
    }
  }
  table {
    border-collapse: collapse;
    font-family: ${pallete.SECONDARY_FONT};
  }
  table td,
  table th {
    border: 1px solid;
    padding: 10px;
    font-size: 14px;
  }

  table th {
    text-align: left;
    font-size: 12px;
    text-transform: uppercase;

    background: #1b1c1f;
  }
`
// const PageLink = styled.div`
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   text-transform: uppercase;
//   border-radius: 4px;

//   transition: 150ms all ease-in;
//   &:hover {
//     transition: 150ms all ease-in;
//   }
// `

// const PaginationContianer = styled.div`
//   margin: 10% 0;
//   font-family: Inconsolata, monospace;
//   display: grid;
//   grid-template-columns: repeat(2, 1fr);
// `
