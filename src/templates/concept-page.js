import React from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

import { graphql } from 'gatsby'
import { MDXProvider } from '@mdx-js/react'
import { MDXRenderer } from 'gatsby-plugin-mdx'

import PostPagination from '../components/pagination/post-pagination'
import Layout from '../components/layout'
import SEO from '../components/seo'
import Intro from '../components/works/intro'
import Image from '../components/works/image'
import DoubleGrid from '../components/elements/double-grid'
import QuadGrid from '../components/elements/quad-grid'
import Back from '../components/pagination/back'

const WorkPage = ({ data, pageContext }) => {
  const shortcodes = { Image, DoubleGrid, QuadGrid }
  const { prev, next, pathPrefix } = pageContext
  const { mdx } = data
  const { frontmatter, body } = mdx

  const {
    title,
    desc,
    role,
    categories,
    image,
    date,
    seo_excerpt,
  } = frontmatter
  return (
    <Layout>
      <SEO title={title} description={seo_excerpt} />
      {/* <Back /> */}
      <Intro
        image={image.childImageSharp.fluid}
        title={title}
        desc={desc}
        date={date}
        role={role}
        categories={categories}
      />

      <WorkContent>
        <MDXProvider components={shortcodes}>
          <MDXRenderer>{body}</MDXRenderer>
        </MDXProvider>
        <div>
          <PostPagination prev={prev} next={next} pathPrefix={pathPrefix} />
        </div>
      </WorkContent>
    </Layout>
  )
}

export default WorkPage

export const PageQuery = graphql`
  query WorkPage($slug: String) {
    mdx(fields: { collection: { eq: "work" }, slug: { eq: $slug } }) {
      body
      frontmatter {
        role
        categories
        title
        desc
        date(formatString: "MMMM YYYY")
        seoTitle
        seoKeywords
        seoImage {
          childImageSharp {
            fluid(maxWidth: 12400, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
        image {
          childImageSharp {
            fluid(maxWidth: 1200, quality: 100) {
              ...GatsbyImageSharpFluid
              src
            }
          }
        }
      }
    }
  }
`
WorkPage.propTypes = {
  pageContext: PropTypes.any,
  pathPrefix: PropTypes.any.isRequired,
  data: PropTypes.shape({
    mdx: PropTypes.shape({
      body: PropTypes.any.isRequired,
      frontmatter: PropTypes.shape({
        role: PropTypes.string.isRequired,
        categories: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
        desc: PropTypes.string.isRequired,
        date: PropTypes.string.isRequired,
        seoTitle: PropTypes.string.isRequired,
        seo_excerpt: PropTypes.string.isRequired,
        image: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
  }).isRequired,
}

const WorkContent = styled.div`
  max-width: 1600px;
  padding: 0 10%;
  margin: 0 auto;
  padding-top: 120px;
  h3 {
    @media screen and (max-width: 767px) {
      font-size: 24px;
    }
  }
`
