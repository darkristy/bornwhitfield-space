import { useEffect, useState } from 'react'

const useElementPosition = el => {
  const getElement = (x, y) => {
    return { x: x, y: y }
  }

  const [elementPosition, setElementPosition] = useState(getElement)

  useEffect(() => {
    const handlePositon = () => {
      let element = el.current
      let x =
        element.getBoundingClientRect().left +
        document.documentElement.scrollLeft +
        element.offsetWidth / 2
      let y =
        element.getBoundingClientRect().top +
        document.documentElement.scrollTop +
        element.offsetHeight / 2

      setElementPosition(getElement(x, y))
    }
    handlePositon()
  }, [el])

  return elementPosition
}

export default useElementPosition
