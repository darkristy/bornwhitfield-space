const { createFilePath } = require(`gatsby-source-filesystem`)
const path = require(`path`)

const blogListPage = path.resolve(`src/pages/archive.js`)
const workListPage = path.resolve(`src/pages/works.js`)

const makePostsFromMdx = async ({ graphql, actions }) => {
  const blogPostTemplate = path.resolve(`src/templates/blog-page.js`)

  const { errors, data } = await graphql(
    `
      {
        allMdx(
          sort: { fields: [frontmatter___date], order: DESC }
          filter: {
            fields: { collection: { eq: "post" } }
            frontmatter: { published: { eq: true } }
          }
        ) {
          edges {
            node {
              id
              body
              fields {
                slug
              }
              frontmatter {
                title
                date
                categories
                published
              }
            }
          }
        }
      }
    `
  )
  if (errors) {
    throw new Error('There was an error')
  }

  const posts = data.allMdx.edges
  posts.forEach((post, i, arr) => {
    const prev = arr[i - 1]
    const next = arr[i + 1]

    actions.createPage({
      path: `/post${post.node.fields.slug}`,
      component: blogPostTemplate,
      context: {
        slug: post.node.fields.slug,
        collection: 'post',
        pathPrefix: '/post',
        prev: prev,
        next: next,
      },
    })
  })
}

const makeWorksFromMdx = async ({ graphql, actions }) => {
  const conceptPage = path.resolve(`src/templates/concept-page.js`)

  const { errors, data } = await graphql(
    `
      {
        allMdx(
          sort: { fields: [frontmatter___date], order: DESC }
          filter: {
            fields: { collection: { eq: "work" } }
            frontmatter: { published: { eq: true } }
          }
        ) {
          edges {
            node {
              id
              body
              fields {
                slug
              }
              frontmatter {
                title
                date
                categories
                published
              }
            }
          }
        }
      }
    `
  )
  if (errors) {
    throw new Error('There was an error')
  }

  const works = data.allMdx.edges

  works.forEach((work, i, arr) => {
    const prev = arr[i - 1]
    const next = arr[i + 1]

    actions.createPage({
      path: `/work${work.node.fields.slug}`,
      component: conceptPage,
      context: {
        slug: work.node.fields.slug,
        collection: 'work',
        pathPrefix: '/work',
        prev: prev,
        next: next,
      },
    })
  })
}

const paginate = async ({
  graphql,
  actions,
  collection,
  pathPrefix,
  component,
}) => {
  const { errors, data } = await graphql(
    `
      {
        allMdx(filter: { fields: { collection: { eq: "${collection}" } } }) {
          totalCount
        }
      }
    `
  )

  const { totalCount } = data.allMdx
  const pages = Math.ceil(totalCount / 10)

  Array.from({ length: pages }).forEach((_, i) => {
    // for each page, use the createPages api to dynamically create that page
    actions.createPage({
      path: `${pathPrefix}${i + 1}`,
      component,
      context: {
        skip: i * 10,
        currentPage: i + 1,
      },
    })
  })
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  await Promise.all([
    makePostsFromMdx({ graphql, actions }),
    paginate({
      graphql,
      actions,
      collection: 'post',
      pathPrefix: '/archive/',
      component: blogListPage,
    }),
    makeWorksFromMdx({ graphql, actions }),
    paginate({
      graphql,
      actions,
      collection: 'work',
      pathPrefix: '/works/',
      component: workListPage,
    }),
  ])
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `Mdx`) {
    // if my posts have a slug in the frontmatter, it means I've specified what I want it to be. Otherwise I want to create one automatically

    // This is where we add our own custom fields to each node
    const generatedSlug = createFilePath({ node, getNode })

    createNodeField({
      name: `slug`,
      node,
      value: node.frontmatter.slug
        ? `/${node.frontmatter.slug}/`
        : generatedSlug,
    })

    // Add it to a collection
    createNodeField({
      name: `collection`,
      node,
      value: getNode(node.parent).sourceInstanceName,
    })
  }
}
