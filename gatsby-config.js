module.exports = {
  siteMetadata: {
    title: `BORNWHITFIELD`,
    description: `This where all the magic lives.`,
    author: `Kahlil Whitfield`,
    socialMedia: {
      twitter: {
        twitterCard: `summary`,
        twitterTitle: `darkristy`,
        twitterDescription: `🏠 my home on the web`,
        twitterImage: ``,
        twitterSite: `@darkristy`,
      },
      facebook: {
        facebookUrl: `https://bornwhitfield.space`,
        facebookTitle: `bornwhitfield`,
        facebookDescription: `🏠 my home on the web`,
        facebookImage: ``,
        facebookType: `website`,
      },
    },
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `@pauliescanlon/gatsby-mdx-embed`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-155843842-1',
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `post`,
        path: `${__dirname}/src/posts`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `work`,
        path: `${__dirname}/src/work`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/assets`,
      },
    },

    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-scroll-indicator`,
      options: {
        // Configure color of the scroll indicator
        color: '#fffffe',
        // Height of the scroll indicator
        height: '3px',
        // Configure paths where the scroll indicator will appear
        paths: ['/post/**', '/work/**'],
        // Configure the z-index of the indicator element
        zIndex: `9999`,
      },
    },

    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        gatsbyRemarkPlugins: [
          `gatsby-remark-reading-time`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 740,
              withWebp: true,
              quality: 80,
            },
          },
          `gatsby-remark-static-images`,
          `gatsby-remark-code-titles`,
          `gatsby-remark-prismjs`,
        ],
      },
    },
    `gatsby-transformer-json`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-copy-linked-files`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
            },
          },
        ],
      },
    },

    `gatsby-plugin-sass`,
    `gatsby-plugin-emotion`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-transition-link`,
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [
          `Inconsolata`,
          `Staatliches`,
          `Ubuntu Mono`,
          `Contrail One`,
          `DM Mono`,
        ],
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `bornwhitfield.space`,
        short_name: `bornwhitfield`,
        start_url: `/`,
        background_color: `#000000`,
        theme_color: `#000000`,
        display: `standalone`,
        icon: `assets/misc/logosmall.svg`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    `gatsby-plugin-offline`,
    `gatsby-plugin-preact`,
  ],
}
